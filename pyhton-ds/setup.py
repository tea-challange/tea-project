from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Data Science approach to solve the TEA Challenge',
    author='earl_gray',
    license='',
)
