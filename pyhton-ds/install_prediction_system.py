from src.models.xgboost_baseline import XGBBaselinePredictor
from src.data import X_DATA_FINAL, Y_DATA_FINAL
import pandas as pd

if __name__ == '__main__':
    X_train = pd.read_pickle(X_DATA_FINAL)
    y_train = pd.read_pickle(Y_DATA_FINAL)

    predictor = XGBBaselinePredictor(precomputed=False)
    predictor.fit(X_train, y_train)  # This also saves the predictor

