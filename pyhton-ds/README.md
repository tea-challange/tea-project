# Introducción


# Instalación

Para el proyecto en Python se debe tener instalado los siguientes paquete en Linux
```
 - python3.4 o +
    - sudo apt-get install python3.6
 - python3-dev
    - sudo apt-get install python3-dev
 - pip 18 o +
   - curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
 - venv (Para el caso que no se quiera mezclar los modulos Python con el sistema)
   - sudo apt install virtualenv
```
  
# Ejecución

Si se quiere utilizar virtualenv se debe ir DENTRO DEL PROYECYO **python-ds** y ejecutar
```
   virtualenv -p python3 venv
   source venv/bin/activate
```

Instalar las dependencias 

```
    pip install -r requirements.txt
```

Realizamos el entrenamiento.
``` 
    python3 install_prediction_system.py 
    
    ESTO PUEDE DEMORAR UNOS 5 min
```
Levantar el servidor Flask

``` 
    export PYTHONPATH=src/

    python3 src/flaskr/server.py 
```



