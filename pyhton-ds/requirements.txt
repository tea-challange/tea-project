#Pip version 18+

# local package
-e .

# external requirements
click
Sphinx
coverage
awscli
flake8
python-dotenv>=0.5.1
flask
numpy
pytz
pandas
jupyterthemes
seaborn
tqdm
sklearn
xgboost
geopy
matplotlib
certifi >= 14.05.14
six >= 1.10
python_dateutil >= 2.5.3
setuptools >= 21.0.0
urllib3 >= 1.15.1
