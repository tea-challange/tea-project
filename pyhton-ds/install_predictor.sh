#!/bin/bash

conda env create -f desafiotea.yml
source activate desafiotea
python -m ipykernel install --user --name desafiotea --display-name "desafiotea"
# pip install .

python install_prediction_system.py
