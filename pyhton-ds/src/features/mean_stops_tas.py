from src.data import PATHS, STOPS_MEAN_TAS
import pandas as pd
from numpy.linalg import norm
import numpy as np

paths = pd.read_pickle(PATHS)
stops_mean_tas = pd.read_pickle(STOPS_MEAN_TAS)


def get_line_pairs(line, paths=paths):
    """ Gets all the consecutive bus stop pairs for the line."""
    path_l = paths[paths.linea == line].copy()
    path_l['next_stop'] = path_l.codigoParada.shift(-1)
    pairs = path_l[['codigoParada', 'next_stop']].iloc[:-1].astype(int)
    return [tuple(x) for x in pairs.values]


def get_all_stop_pairs(paths):
    """ Gets the consecutive bus stop pairs for all lines."""
    pairs = set()
    for line in paths.linea.unique():
        pairs |= set(get_line_pairs(line, paths))
    return list(pairs)


def get_mean_pair_ta(stop_tuple, data):
    """ Get the mean TA for a pair of bus stops. """
    stop1, stop2 = stop_tuple

    stop1_coords = paths[paths.codigoParada == stop1][['x', 'y']].values[0]
    valid_data = data[data.bus_stop == stop2]
    valid_data = valid_data[norm(valid_data[['x', 'y']] - stop1_coords, axis=1) < 50]
    deltas = (valid_data.ta - valid_data.timestamp).apply(lambda x: x.total_seconds())

    return deltas.mean()


def get_mean_stop_tas(stop_pairs, data):
    """ Get the mean TAs for a list of stops pairs. """
    pairs_df = pd.DataFrame(stop_pairs, columns=['stop_0', 'stop_1'])
    pairs_df['mean_ta'] = pairs_df.apply(
        lambda x: get_mean_pair_ta(x.values, data=data), axis=1)
    return pairs_df


def summed_ta(stop1, stop2, line, mean_tas=stops_mean_tas, paths=paths):
    """ Calculate the sum of the mean tas in the path from stop1 to stop2. """
    if stop1 == stop2:
        return 0.0
    line_path = paths[paths.linea == line]
    ordinal1 = line_path[line_path.codigoParada == stop1].ordinal.values[0]
    ordinal2 = line_path[line_path.codigoParada == stop2].ordinal.values[0]
    ordinals = sorted([ordinal1, ordinal2])
    sub_path = line_path[line_path.ordinal.isin(range(ordinals[0], ordinals[1] + 1))]
    pairs = get_all_stop_pairs(sub_path)
    deltas = pd.DataFrame([mean_tas.loc[pair] for pair in pairs])
    if deltas.empty:
        return np.nan

    return deltas.sum().values[0]


def summed_ta_record(record):
    """ Another interface for summed_ta. """
    return summed_ta(record.nearest_stop, record.bus_stop, record.linea)
