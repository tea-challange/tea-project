from sklearn.preprocessing import LabelEncoder
import pandas as pd
import geopy.distance

from src.data import PATHS


def preprocess_xgboost(X, y, paths):
    """
    Preprocesses the features / targets for use in XGBoost.
    The targets are differences of time (between the ta and the current timestamp).
    """
    pp_feats = ['distance', 'x', 'y', 'calle', 'esquina', 'ordinal', 'x_s', 'y_s', 'nearest_stop']
    ret_feats = ['timestamp', 'linea', 'bus_stop', 'codigoBus', 'lat', 'long', 'nearest_stop']

    if y is not None:
        X_pp = X.join(y)
        X_pp['delta'] = (X_pp['ta'] - X_pp['timestamp']).apply(lambda x: x.seconds)
    else:
        X_pp = X

    paths_pp = paths.drop(['lat', 'long'], axis=1).rename(columns={'x': 'x_s',
                                                                   'y': 'y_s'})
    merged = X_pp.merge(paths_pp, left_on=['bus_stop', 'linea'],
                        right_on=['codigoParada', 'linea'])

    if y is not None:
        y_pp = merged['delta']
    else:
        y_pp = None

    X_pp = merged[pp_feats]
    X_ret = merged[ret_feats]

    return X_pp, y_pp, X_ret


def get_streets_encoder(paths):
    """ Get a label encoder for all the 'street' features. """
    streets = pd.concat([paths.calle, paths.esquina]).unique()
    encoder = LabelEncoder()
    encoder.fit(streets)
    return encoder


def encode_streets(X, encoder):
    """ Use a label encoder to transform the 'street' features. """
    X[['calle', 'esquina']] = X[['calle', 'esquina']].apply(encoder.transform)
    return X


def decode_streets(X, encoder):
    """ Use a label encoder to decode the 'street' features. """
    X[['calle', 'esquina']] = X[['calle', 'esquina']].apply(encoder.inverse_transform)
    return X
