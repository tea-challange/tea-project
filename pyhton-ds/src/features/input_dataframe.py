from src.data.create_from_sim import parse_record
import src.data.target_creation as tc
import src.evaluation.eval_creation as ec
import pandas as pd
import os
from src.data import PATHS, DATA_RAW, DATA_INTERIM
from tqdm import tqdm
import json
import src.features.mean_stops_tas as mst

DATA_TXT = os.path.join(DATA_RAW, 'raw_data.txt')
paths = pd.read_pickle(PATHS)


def input_to_df(data_in):
    """
    Returns a DataFrame with all the relevant data from one
    timestamp.

    Args:
        data_in (list(dict)): A list of dictionaries with the same
            structure as the JSON files that the Orion service sends.

    Returns:
        pd.DataFrame: A dataframe with the parsed data
    """
    data = list()
    for record in data_in:
        for sample in record['data']:
            data_line = parse_record(sample)
            data.append(data_line)

    data_df = pd.DataFrame(data)
    data_df = data_df.drop_duplicates()
    data_df = data_df[~((data_df.long == 0) | (data_df.lat == 0))]  # Filter some zeros

    return data_df


def feat_nearest_stop(X):
    """
    Find the nearest stop for each record.
    Args:
        X (pd.DataFrame): Must have the distances to all the bus stops
            in its line path.
    Returns:
        pd.DataFrame: The same as the input with the nearest stop feature added.
    """
    X['nearest_stop'] = X[X.columns[
        X.columns.str.contains('d_')]
                         ].idxmin(axis=1).apply(lambda x: int(x.split('_')[1]))
    return X


def feat_distances(data):
    """ Add the distances feature to the dataframe. """
    wrapped = tc.process_data_distances(
        data, paths).reset_index(drop=True).drop('id', axis=1)
    wrapped = feat_nearest_stop(wrapped)

    return ec.unwrap_data(wrapped, has_tas=False).reset_index(drop=True)


def feat_distances_and_tas(data):
    """ Add the distances feature to the dataframe. """
    print('getting distances')
    wrapped = tc.process_data_distances(
        data, paths).reset_index(drop=True).drop('id', axis=1)
    wrapped.to_pickle(os.path.join(DATA_INTERIM, 'data_temp.pkl'))
    print('nearest stop')
    wrapped = feat_nearest_stop(wrapped)
    wrapped.to_pickle(os.path.join(DATA_INTERIM, 'data_temp.pkl'))
    print('target creation')
    wrapped = tc.process_data_tas(wrapped)
    wrapped.to_pickle(os.path.join(DATA_INTERIM, 'data_temp.pkl'))

    data = ec.unwrap_data(wrapped, has_tas=True).reset_index(drop=True)
    return data.dropna(subset=['ta'])


def feat_mean_tas(data):
    """
    Adds the mean ta, from the nearest bus stop to the 'target' bus stop.
    The TA is calculated as the sum of the mean TAs for each stop-to-stop segment
    in the corresponding line.
    """
    data['mean_ta'] = data.apply(mst.summed_ta_record, axis=1)
    return data


def preprocess(data):
    """
    Returns a DataFrame with all the relevant data from one
    timestamp, and the added features for XGBoost.

    Args:
        data_in (list(dict)): A list of dictionaries with the same
            structure as the JSON files that the Orion service sends.

    Returns:
        pd.DataFrame: A dataframe with the parsed data
    """
    data = input_to_df(data)
    data = feat_distances(data)
    # data = feat_mean_tas(data)

    return data


def parse_file(filepath=DATA_TXT):
    """ Returns a DataFrame with all the relevant data from a txt input file. """
    with open(filepath, 'r') as file:
        lines = file.readlines()
    data = map(lambda x: json.loads(x.replace('\n', '')), lines)
    print('parsing data')
    data = input_to_df(data)
    data.to_pickle(os.path.join(DATA_INTERIM, 'data_temp.pkl'))
    return feat_distances_and_tas(data)
