import pandas as pd
from src.data import X_TRAIN, X_VAL, Y_TRAIN, VAL_SET, TEST_SET, X_TEST, Y_VAL
import src.utils as utils
from sklearn.metrics import mean_squared_error as mse


def query_samples(samples, predictor):
    """ Auxiliary function to query the real tas of samples. """
    rows = []
    for index, row in samples.iterrows():
        new_row = row.copy()
        new_row['ta'] = predictor.query(row.stop, row.line, row.timestamp)
        rows.append(new_row)
    return pd.DataFrame(rows)


def query_samples_pred(samples, predictor, colname='tea'):
    """ Auxiliary function to query the predicted tas of samples. """
    rows = []
    for index, row in samples.iterrows():
        new_row = row.copy()
        new_row[colname] = predictor.query(row.stop, row.line, row.timestamp)['tea']
        rows.append(new_row)
    return pd.DataFrame(rows)


def evaluate_predictor(predictor,
                       parallel_query_fun,
                       test=False,
                       x_train_path=X_TRAIN,
                       x_val_path=X_VAL,
                       x_test_path=X_TEST):
    """
    Evaluates a predictor. Returns the MSE error, and a dataframe with the predictions.
    Note: It gets the data from the default paths.
    """
    # Get the data
    X_train = pd.read_pickle(x_train_path)
    X_val = pd.read_pickle(x_val_path)
    y_train = pd.read_pickle(Y_TRAIN)

    if test:
        X_train = pd.concat([X_train, X_val])
        X_val = pd.read_pickle(x_test_path)
        y_val = pd.read_pickle(Y_VAL)
        y_train = pd.concat([y_train, y_val])
        val_set = pd.read_pickle(TEST_SET)
    else:
        val_set = pd.read_pickle(VAL_SET)

    print('Fitting')
    predictor.fit(X_train, y_train)

    print('Evaluating')
    y_val_pred = list()
    for idx, time in enumerate(val_set.timestamp.sort_values().unique()):
        timestamp = pd.Timestamp(time)
        print('Evaluating in timestamp No {}: {}'.format(idx, timestamp))
        X_val_t = X_val[X_val.timestamp == timestamp]
        val_set_t = val_set[val_set.timestamp == timestamp]

        # Call update: That is, 15 seconds have passed in the real time.
        predictor.update(X_val_t)

        # Query all the lines and all the corresponding bus stops
        samples = utils.apply_parallel(val_set_t.groupby(val_set_t.index), parallel_query_fun)
        y_val_pred.append(samples.reset_index(drop=True))

    results = pd.concat(y_val_pred)
    y_true = results.ta.values
    y_pred = results.tea.values
    error = mse(y_true, y_pred)

    return error, results
