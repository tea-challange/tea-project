import src.data.target_creation as tc
from src.data import PATHS
import src.utils as utils
import pandas as pd

paths = pd.read_pickle(PATHS)


def unwrap_record(record, paths, w_stops, has_tas=True):
    data_list = list()
    basic_record = record.drop(record.index[record.index.str.contains('d_')])
    for stop in w_stops:
        rdict = basic_record.to_dict()
        rdict.update({'bus_stop': stop})
        d_col = 'd_{}'.format(stop)
        if has_tas:
            ta_col = 'd_{}_ta'.format(stop)
            rdict.update({'distance': record[d_col], 'ta': record[ta_col]})
        else:
            rdict.update({'distance': record[d_col]})
        data_list.append(rdict)
    return pd.DataFrame(data_list)


def unwrap_line(data, paths, **kwargs):
    """
    Gets the data for one line and unwraps it.
    Args:
        data (pandas.DataFrame): Contains the data only for one line.
    """
    line = data.linea.iloc[0]
    # print('Processing line {}'.format(line))
    w_stops = tc.relevant_stops(paths, line).codigoParada.tolist()
    df_list = list()
    for index, row in data.iterrows():
        df_list.append(unwrap_record(row, paths, w_stops, **kwargs))

    return pd.concat(df_list)


def aux_unwrap_line_no_tas(x):
    return unwrap_line(x, paths, has_tas=False)


def aux_unwrap_line_tas(x):
    return unwrap_line(x, paths, has_tas=True)


def unwrap_data(data, has_tas=True):
    """
    Takes a dataframe with initial data, distances and times of arrival, and
    returns a new dataframe with one target bus stop per record.

    Args:
        data (pandas.DataFrame): A dataframe that contains basic features +
        distances to all bus stops + times of arrival to all bus stops, in its
        columns.
    Returns:
        pandas.DataFrame: The unwrapped dataframe.
    """
    if has_tas:
        aux_fun = aux_unwrap_line_tas
    else:
        aux_fun = aux_unwrap_line_no_tas

    return utils.apply_parallel(data.groupby('linea'), aux_fun)


def separate_target(data):
    """ Returns X, y  (feats, target)"""
    return data.drop('ta', axis=1), data['ta']
