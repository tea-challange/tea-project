from itertools import product, chain
import pandas as pd
import numpy as np
import itertools
import src.data.target_creation as tc


class Oracle(object):
    """
    A class that stores the real labels (times of arrival),
    and can be used to retrieve the real t.a. for a query.
    Args:
        X (pd.DataFrame): Contains the input features.
            It should contain all the data known so far
            (typically it contains train and val data).
        y (pd.Series): Contains the actual times of arrival.
            It should contain all the data known so far
            (typically it contains train and val data).
    """

    def __init__(self, data, X_val, paths):
        self.paths = paths
        self.X_val = X_val.copy()
        self.data = data.copy()
        self.cache = self.data
        self.lines = paths.copy().linea.unique().tolist()

    @staticmethod
    def query_data(stop, line, current_time, data):
        valid_data = data[(data.linea == line)
                          & (data.bus_stop == stop)
                          & (data.ta >= current_time)]
        if valid_data.empty:
            return np.nan

        min_bus = valid_data[valid_data.ta == valid_data.ta.min()]
        return (pd.Timestamp(min_bus['ta'].values[0]) - current_time).seconds

    def query(self, stop, line, current_time):
        return self.query_data(stop, line, current_time, self.data)

    def query_forward(self, stop, line, current_time):
        self.time_forward(current_time)
        return self.query_data(stop, line, current_time, self.cache)

    def time_forward(self, current_time):
        """
        Updates the cache with the records that have t.a.s in the future.
        It cannot be run for times in the past (only use this sequentially).
        The idea is to make the queries faster.
        """
        self.cache = self.cache[self.cache.ta >= current_time]

    def get_eval_set(self, num_samples=100):
        line_stop = [list(product([line], tc.relevant_stops(self.paths, line).codigoParada.tolist()))
                     for line in self.lines]
        line_stop = list(chain(*line_stop))
        line_stop = np.array(line_stop)

        np.random.seed(seed=2018)

        ls_samples = line_stop[np.random.choice(np.arange(line_stop.shape[0]),
                                                size=min(num_samples, len(line_stop)),
                                                replace=False)]
        t_samples = [pd.Timestamp(t) for t in self.X_val.timestamp.unique()]
        samples = np.array(list(itertools.product(t_samples, ls_samples)))

        samples = pd.DataFrame([[a, int(b[1]), b[0]] for a, b in samples],
                               columns=['timestamp', 'stop', 'line'])
        return samples