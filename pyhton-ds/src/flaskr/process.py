#!/usr/bin/python
# -*- coding: utf8 -*-

from multiprocessing import Process
import time
from queue import Empty

MIN_SIZE_NEW_DATA_TO_RETRAIN = 100
MAX_WAITING_TIME = 2


class OnlineTrainingWorker(Process):
    """
    """

    def __init__(self, queue, wid, predictor):
        """

        :param queue:
        :param wid:
        """
        super(OnlineTrainingWorker, self).__init__()
        self.queue = queue
        self.wid = wid
        self.predictor = predictor

        self.cached_data = []
        self.timer = 0

    def run(self):

        while True:
            # Lee de la cola un lote de Items

            try:

                msg = self.queue.get(timeout=MAX_WAITING_TIME)  # Operación bloqueante
                self.cached_data.append(msg)
                self.send_data()
            except Empty:
                if len(self.cached_data) > 0:
                    self.send_data()
                else:
                    print("timeout without data")

    def send_data(self):
        print("OnlineTrainningWorker: new bus event")

        if self.timer == 0:
            self.timer = time.time()
            diff_t = 0
        else:
            diff_t = time.time() - self.timer

        # TODO: Refinar si queremos recivir la señal termine y matar al proceso
        # if msg == "FIN":
        #     # Pone de nuevo el mensaje para que lo vean otros workers
        #     self.queue.put("FIN")
        #     print("Extractor {}: Finalizando Extracción".format(self.wid))
        #     # break

        # TODO: agregar wrapper que necesita Miguel

        # TODO: encolar

        if len(self.cached_data) > MIN_SIZE_NEW_DATA_TO_RETRAIN or diff_t >= MAX_WAITING_TIME:
            self.timer = time.time()  # restart timer
            print("OnlineTrainningWorker: Updating predictor starts...")
            print(self.cached_data)
            print(type(self.cached_data))
            self.predictor.update(self.cached_data)  # FIXME: Esta bien data?
            print("OnlineTrainningWorker: Updating predictor finish!")
            self.cached_data = []  # clean cached data
