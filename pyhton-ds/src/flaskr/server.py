from flask import Flask,request, jsonify

from src.models.xgboost_baseline import XGBBaselinePredictor
from src.data import X_TRAIN
from src.data import Y_TRAIN

from multiprocessing import Process, Queue
from src.flaskr.process import OnlineTrainingWorker

from models.responder import Responder

import pandas
import time
import json

app = Flask(__name__)

predictor = XGBBaselinePredictor.load_predictor()

responder = Responder()

QUEUE_SIZE = 200  # Should be property
queue = Queue(maxsize=QUEUE_SIZE)


@app.route('/status')
def status():
    return "OK"


@app.route('/')
def home():
    return "Tea Challange"


@app.route('/query/<line>/<int:stop>')
def query(line, stop):
    """     stop (int): The bus stop integer code.
            line (str): The line string code.
            current_time (pandas.Timestamp): The current time. """
    time_pd = pandas.Timestamp.now()
    return responder.query(stop, line, time_pd)


@app.route('/fit')
def fit():
    X_train = pandas.read_pickle(X_TRAIN)
    Y_train = pandas.read_pickle(Y_TRAIN)

    predictor.fit(X_train, Y_train)
    return "FIT Complete"


@app.route('/update',methods=['POST'])
def update():
    body = request.get_json()

    wrapper = {
        "subscriptionId": "dummy",
        "data": [body]
    }

    queue.put(wrapper)
    return "update"


def init_server():
    online_training_wrk = OnlineTrainingWorker(queue=queue, wid=1, predictor=predictor)
    online_training_wrk.start()

if __name__ == '__main__':
    init_server()
    app.run(debug=True)
