import pandas as pd


def smooth_update(old_record, new_record, learning_rate=0.1):
    coherence_period = 70
    if old_record is None:
        return new_record
    if new_record is None:
        return old_record
    delta_t = (new_record.timestamp - old_record.timestamp).seconds
    if delta_t < 0:  # The order is not OK
        return old_record  # In fact, it is the newest
    if delta_t > coherence_period:
        return new_record
    else:
        ret_record = new_record.copy()
        ret_record['tea'] = pd.Timestamp.fromtimestamp(
            old_record.tea.timestamp() * (1 - learning_rate)
            + new_record.tea.timestamp() * learning_rate)
        return ret_record


def smooth_path(path_df, **kwargs):
    sm_list = []
    prev_row = None
    for index, row in path_df.iterrows():
        sm_record = smooth_update(prev_row, row, **kwargs)
        sm_list.append(sm_record)
        prev_row = sm_record
    return pd.DataFrame(sm_list)


def smooth_path_value(path_df, **kwargs):
    prev_row = None
    sm_record = None
    for index, row in path_df.iterrows():
        sm_record = smooth_update(prev_row, row, **kwargs)
        prev_row = sm_record
    return sm_record
