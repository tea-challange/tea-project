import sqlite3
from sqlite3 import Error
from src.models.xgboost_baseline import DB_PATH, ID_LINEA, ID_BUS, ID_PARADA, LOCATION, TEA
import pandas as pd
import src.models.smoother as smoother
import src.features.input_dataframe as input_d
import datetime as dt


class Responder(object):

    def __init__(self, db_path=DB_PATH):
        self.db_path = db_path
        self.delta_max = 3600.0 * 3.0

    def query(self, stop, line, current_time):
        """
        Predict the time of arrival to the bus stop 'stop' for the next bus of
        the line 'line'.

        Args:
            stop (int): The bus stop integer code.
            line (str): The line string code.
            current_time (pandas.Timestamp): The current time.

        Returns:
            dict: A dictionary with:
            ['id_linea': int, 'id_parada': int, 'id_bus': string,
            'location': GeoJSON, 'tea': float (seconds until arrival)]
        """
        default_ret = {}

        sql_query = 'select * from incoming where (linea = {}) and (bus_stop = {})'.format(line, stop)

        conn = None
        try:
            conn = sqlite3.connect(self.db_path)
            valid_buses = pd.read_sql(sql_query, conn,
                                      parse_dates={'timestamp': '%Y-%m-%dT%H:%M:%S',
                                                   'tea': '%Y-%m-%dT%H:%M:%S'})
            if valid_buses.empty:
                return default_ret
            valid_buses = valid_buses[valid_buses.tea > current_time]
            if valid_buses.empty:
                return default_ret

            # Fallback system
            valid_buses = self.fallback(valid_buses)

            # Smooth the predictions
            valid_buses = valid_buses.groupby(
                ['linea', 'bus_stop', 'codigoBus']).apply(
                smoother.smooth_path_value).reset_index(drop=True)

            next_bus = valid_buses[valid_buses.tea == valid_buses.tea.min()].iloc[0]
            return self.format_output(next_bus, current_time)
        except Error as e:
            return default_ret
        finally:
            conn.close()

    def fallback(self, data, low=0.5, high=1.5):
        """
        Check if the tea is too far away from the mean summed tea.
        If it is too far away return the mean summed tea as tea.
        """
        data = input_d.feat_mean_tas(data)
        outliers = (data.delta > high * data.mean_ta) | (data.delta < low * data.mean_ta)
        outliers = outliers & (data.mean_ta < self.delta_max)
        if outliers.sum() > 0:
            data.loc[outliers, 'delta'] = data.loc[outliers, 'mean_ta']
            data.loc[outliers, 'tea'] = data.loc[outliers, 'timestamp'] + data.loc[outliers, 'delta'].apply(
                lambda x: dt.timedelta(seconds=x))
        return data

    @staticmethod
    def create_geojson(x, y):
        """ Create a GeoJSON from coordinates. """
        return {'type': 'geo:json',
                'value': {'type': 'Point',
                          'coordinates': [x, y]},
                'metadata': {}}

    def format_output(self, raw_output, current_time):
        """
        Formats the output of the query function, to fit the requested format.

        Args:
            raw_output (pd.Series): A row of self.data that was selected as
                the best answer.
        Returns:
            dict: A dictionary that contains all the requested data for a query.
        """
        return {ID_LINEA: raw_output.linea,
                ID_PARADA: raw_output.bus_stop,
                ID_BUS: raw_output.codigoBus,
                LOCATION: self.create_geojson(
                    raw_output.long, raw_output.lat),
                TEA: (raw_output.tea - current_time).seconds}
