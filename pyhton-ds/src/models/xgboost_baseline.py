import os
import pandas as pd
from src.data import ROOT_DIR, PATHS, DATA_PROCESSED
from src.features.build_features import preprocess_xgboost, get_streets_encoder, encode_streets
import xgboost as xgb
import datetime as dt
import pickle
import src.features.input_dataframe as input_d
import sqlite3
from sqlite3 import Error


XGB_BASELINE_PATH = os.path.join(ROOT_DIR, 'models', 'xgb_baseline.pkl')
ID_LINEA = 'id_linea'
ID_PARADA = 'id_parada'
ID_BUS = 'id_bus'
LOCATION = 'location'
TEA = 'tea'
DB_PATH = os.path.join(DATA_PROCESSED, 'develop.db')


class XGBBaselinePredictor(object):
    """
    Baseline XGBoost predictor

    Args:
        filepath (str): The filepath to save this predictor.

    Attributes:
        dummy_delta (float): The constant value to predict for all samples.
            This is a value in seconds that is added to the current timestamp.
    """

    def __init__(self, paths=PATHS, filepath=XGB_BASELINE_PATH, precomputed=True, db_path=DB_PATH):
        self.db_path = db_path
        self.precomputed = precomputed
        self.filepath = filepath
        self.model = xgb.XGBRegressor(seed=2018, n_jobs=4)
        self.encoder = None
        self.paths = pd.read_pickle(paths)
        self.incoming_buses = None
        self.delta_max = 3600.0 * 3.0

    def fit(self, X, y):
        """
        Fit the training data. Only to be run offline.

        Args:
            X (pandas.DataFrame): Contains the training features. The columns are,
                in principle:
                [bus_stop, codigoBus, distance, lat, linea, long, timestamp, uid, x, y]
            y (pandas.Series): Contains the times of arrival ('ta'). It's type is pd.Timestamp.
        """
        X_pp, y_pp, _ = preprocess_xgboost(X, y, self.paths)
        self.encoder = get_streets_encoder(self.paths)
        X_pp = encode_streets(X_pp, self.encoder)
        self.model.fit(X_pp, y_pp)

        self.save_predictor()

    def update(self, X):
        """
        Update the training. This function is called each time there is a new datum
        available from the simulator.

        Args:
            X (pandas.DataFrame): Contains the training features in the columns.
                Each row is a sample. The columns are, in principle:
                [bus_stop, codigoBus, distance, lat, linea, long, timestamp, uid, x, y]
        """
        # TODO: Incremental learning with the xgb_model param.

        # Get features
        if not self.precomputed:
            X = input_d.preprocess(X)

        # Predict
        X_val_pp, y_val_pp, X_val_ret = preprocess_xgboost(X, None, self.paths)
        X_val_pp = encode_streets(X_val_pp, self.encoder)
        y_val_pred = self.model.predict(X_val_pp)
        X_val_ret['delta'] = y_val_pred
        X_val_ret['tea'] = X_val_ret['timestamp'] + X_val_ret.delta.apply(
            lambda x: dt.timedelta(seconds=x))

        # Fill outliers with the max value
        # X_val_ret = X_val_ret[X_val_ret.delta < self.delta_max]
        outliers = X_val_ret.delta > self.delta_max
        X_val_ret.loc[outliers, 'tea'] = self.delta_max

        if self.incoming_buses is None:
            self.incoming_buses = X_val_ret
        else:
            self.incoming_buses = pd.concat([self.incoming_buses, X_val_ret])

        # Smooth the paths in time
        # self.smooth_preds()

        # Remove duplicates (possible bug fix)
        self.incoming_buses = self.incoming_buses.drop_duplicates(
             subset=['bus_stop', 'linea', 'codigoBus'], keep='last')
        self.to_database(self.incoming_buses, 'incoming', if_exists='replace', index=False)

    def query(self, stop, line, current_time):
        """
        Predict the time of arrival to the bus stop 'stop' for the next bus of
        the line 'line'.

        Args:
            stop (int): The bus stop integer code.
            line (str): The line string code.
            current_time (pandas.Timestamp): The current time.

        Returns:
            dict: A dictionary with:
            ['id_linea': int, 'id_parada': int, 'id_bus': string,
            'location': GeoJSON, 'tea': float (seconds until arrival)]
        """
        default_ret = {}
        valid_buses = self.incoming_buses[(self.incoming_buses.linea == line)
                                          & (self.incoming_buses.bus_stop == stop)
                                          & (self.incoming_buses.tea > current_time)]
        if valid_buses.empty:
            return default_ret

        next_bus = valid_buses[valid_buses.tea == valid_buses.tea.min()].iloc[0]
        return self.format_output(next_bus, current_time)

    def save_predictor(self):
        """ Save this object in pickle format. """
        with open(self.filepath, 'wb') as file:
            pickle.dump(self, file)

    def to_database(self, df, table, if_exists='replace', index=False):
        """ Sends a dataframe to the default database. """
        conn = None
        try:
            conn = sqlite3.connect(self.db_path)
            df.to_sql(table, con=conn, if_exists=if_exists, index=index)
        except Error as e:
            print(e)
        finally:
            conn.close()

    @classmethod
    def load_predictor(cls, filepath=XGB_BASELINE_PATH):
        """ Save a predictor from pickle format. """
        with open(filepath, 'rb') as file:
            predictor = pickle.load(file)
        return predictor

    @staticmethod
    def create_geojson(x, y):
        """ Create a GeoJSON from coordinates. """
        return {'type': 'geo:json',
                'value': {'type': 'Point',
                          'coordinates': [x, y]},
                'metadata': {}}

    def format_output(self, raw_output, current_time):
        """
        Formats the output of the query function, to fit the requested format.

        Args:
            raw_output (pd.Series): A row of self.data that was selected as
                the best answer.
        Returns:
            dict: A dictionary that contains all the requested data for a query.
        """
        return {ID_LINEA: raw_output.linea,
                ID_PARADA: raw_output.bus_stop,
                ID_BUS: raw_output.codigoBus,
                LOCATION: self.create_geojson(
                    raw_output.long, raw_output.lat),
                TEA: (raw_output.tea - current_time).seconds}

    @staticmethod
    def smooth_update(old_record, new_record, learning_rate=0.1):
        coherence_period = 70
        if old_record is None:
            return new_record
        if new_record is None:
            return old_record
        delta_t = (new_record.timestamp - old_record.timestamp).seconds
        if delta_t < 0:  # The order is not OK
            return old_record  # In fact, it is the newest
        if delta_t > coherence_period:
            return new_record
        else:
            ret_record = new_record.copy()
            ret_record['tea'] = pd.Timestamp.fromtimestamp(
                old_record.tea.timestamp() * (1 - learning_rate)
                + new_record.tea.timestamp() * learning_rate)
            return ret_record

    @staticmethod
    def smooth_path(path_df, **kwargs):
        sm_list = []
        prev_row = None
        for index, row in path_df.iterrows():
            sm_record = XGBBaselinePredictor.smooth_update(prev_row, row, **kwargs)
            sm_list.append(sm_record)
            prev_row = sm_record
        return pd.DataFrame(sm_list)

    @staticmethod
    def smooth_path_value(path_df, **kwargs):
        prev_row = None
        sm_record = None
        for index, row in path_df.iterrows():
            sm_record = XGBBaselinePredictor.smooth_update(prev_row, row, **kwargs)
            prev_row = sm_record
        return sm_record

    def smooth_preds(self):
        self.incoming_buses = self.incoming_buses.groupby(
            ['linea', 'bus_stop', 'codigoBus']).apply(
            XGBBaselinePredictor.smooth_path_value).reset_index(drop=True)
