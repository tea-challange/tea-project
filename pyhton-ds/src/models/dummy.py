import os
import pickle
import datetime as dt
import pandas as pd
from src.data import ROOT_DIR
import json

DUMMY_PATH = os.path.join(ROOT_DIR, 'models', 'dummy.pkl')
ID_LINEA = 'id_linea'
ID_PARADA = 'id_parada'
ID_BUS = 'id_bus'
LOCATION = 'location'
TEA = 'tea'


class DummyPredictor(object):
    """
    A dummy predictor for the times of arrival. Just to test the interfaces.

    Args:
        filepath (str): The filepath to save this predictor.

    Attributes:
        dummy_delta (float): The constant value to predict for all samples.
            This is a value in seconds that is added to the current timestamp.
    """

    def __init__(self, filepath=DUMMY_PATH):
        self.filepath = filepath
        self.dummy_delta = None

    def fit(self, X, y):
        """
        Fit the training data. Only to be run offline.

        Args:
            X (pandas.DataFrame): Contains the training features. The columns are,
                in principle:
                [bus_stop, codigoBus, distance, lat, linea, long, timestamp, uid, x, y]
            y (pandas.Series): Contains the times of arrival ('ta'). It's type is pd.Timestamp.
        """
        temp_data = X.copy()
        temp_data['delta'] = (y - temp_data['timestamp']).apply(lambda x: x.seconds)
        self.dummy_delta = temp_data.delta.mean()

        self.save_predictor()

    def update(self, X):
        """
        Update the training. This function is called each time there is a new datum
        available from the simulator.

        Args:
            X (pandas.DataFrame): Contains the training features in the columns.
                Each row is a sample. The columns are, in principle:
                [bus_stop, codigoBus, distance, lat, linea, long, timestamp, uid, x, y]
        """
        pass

    def query(self, stop, line, current_time):
        """
        Predict the time of arrival to the bus stop 'stop' for the next bus of
        the line 'line'.

        Args:
            stop (int): The bus stop integer code.
            line (str): The line string code.
            current_time (pandas.Timestamp): The current time.

        Returns:
            dict: A dictionary with:
            ['id_linea': int, 'id_parada': int, 'id_bus': string,
            'location': GeoJSON, 'tea': float (seconds until arrival)]
        """
        default_ret = {ID_LINEA: line,
                       ID_PARADA: stop,
                       ID_BUS: 0,
                       LOCATION: self.create_geojson(0, 0),
                       TEA: self.dummy_delta}
        return json.dumps(default_ret, ensure_ascii=False)

    def save_predictor(self):
        """ Save this object in pickle format. """
        with open(self.filepath, 'wb') as file:
            pickle.dump(self, file)

    @classmethod
    def load_predictor(cls, filepath=DUMMY_PATH):
        """ Save a predictor from pickle format. """
        with open(filepath, 'r') as file:
            predictor = pickle.load(file)
        return predictor

    @staticmethod
    def create_geojson(x, y):
        """ Create a GeoJSON from coordinates. """
        return {'type': 'geo:json',
                'value': {'type': 'Point',
                          'coordinates': [x, y]},
                'metadata': {}}

    def format_output(self, raw_output, current_time):
        """
        Formats the output of the query function, to fit the requested format.

        Args:
            raw_output (pd.Series): A row of self.data that was selected as
                the best answer.
        Returns:
            dict: A dictionary that contains all the requested data for a query.
        """
        return {ID_LINEA: raw_output.linea.iloc[0],
                ID_PARADA: raw_output.bus_stop.iloc[0],
                ID_BUS: raw_output.codigoBus.iloc[0],
                LOCATION: self.create_geojson(
                    raw_output.long.iloc[0], raw_output.lat.iloc[0]),
                TEA: (raw_output.tea.iloc[0] - current_time).seconds}
