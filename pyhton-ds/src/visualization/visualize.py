import matplotlib.pyplot as plt
from src.data.target_creation import  get_bus_path


def show_path(data, paths, uid):
    """ Plots the path of one uid. """
    line = uid.split('_')[0]
    sub_paths = paths[paths.linea == line]
    stops = sub_paths.groupby('codigoParada').mean().drop('ordinal', axis=1)
    plt.scatter(stops.x, stops.y)
    path = get_bus_path(data, uid)
    plt.plot(path.x, path.y, '.')