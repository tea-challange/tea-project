""" Utility functions. """
from multiprocessing import Pool, cpu_count
import pandas as pd


def common_values(series1, series2):
    """ Shows the differences, intersections and union of two sets. """
    values1 = set(series1)
    values2 = set(series2)
    intersection = set.intersection(values1, values2)
    no_values2 = values1 - values2
    no_values1 = values2 - values1
    total = set.union(values1, values2)

    print('Intersection: {}'.format(len(intersection)))
    print('Total set 1: {}'.format(len(values1)))
    print('Not in set 2: {}'.format(len(no_values2)))
    print('Total set 2: {}'.format(len(values2)))
    print('Not in set 1: {}'.format(len(no_values1)))
    print('Total: {}'.format(len(total)))


def apply_parallel(grouped, func):
    names, groups = zip(*grouped)
    # print(names)
    num_cpus = min(cpu_count(), len(grouped))

    with Pool(num_cpus) as p:
        ret_list = p.map(func, groups)

    for name, res in zip(names, ret_list):
        res['group_index'] = name
        res.set_index(['group_index'], append=True, inplace=True)

    return pd.concat(ret_list)


def is_df_sorted(df, colname):
    return pd.Index(df[colname]).is_monotonic
