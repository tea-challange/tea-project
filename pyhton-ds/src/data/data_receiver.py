from http.server import BaseHTTPRequestHandler, HTTPServer

PORT_NUMBER = 8080


class MyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        print('Got a request!')
        print('headers: ')
        print(str(self.headers))
        print('path: ')
        print(str(self.path))
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write("Hello World!".encode('utf-8'))
        return

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])  # Gets the size of data
        post_data = self.rfile.read(content_length)  # Gets the data itself

        with open("raw_data.txt", "a") as file:
            file.write(post_data.decode('utf-8') + '\n')

        print("POST request,\nPath: {}\nHeaders:\n{}\n\nBody:\n{}\n".format(
              str(self.path), str(self.headers), post_data.decode('utf-8')))


server = HTTPServer(('', PORT_NUMBER), MyHandler)
try:
    print('Started httpserver on port {}'.format(PORT_NUMBER))
    server.serve_forever()

except KeyboardInterrupt:
    print('^C received, shutting down the web server')
    server.socket.close()
