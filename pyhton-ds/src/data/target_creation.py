from src import utils
import numpy as np
import pandas as pd
from tqdm import tqdm
from numpy.linalg import norm
from src.data import PATHS
from time import time


paths_s = pd.read_pickle(PATHS)


def process_data_tas(wrapped):
    target_data = utils.apply_parallel(wrapped.groupby('uid'), fill_one_uid_para)
    return target_data.reset_index().drop('group_index', axis=1)


def process_data_distances(data, paths=paths_s):
    """
    Returns a dataframe that is equal to 'data' but has the distances to all the bus stops merged.
    The only distances that are really calculated are those that are in the path of the line of each
    record.
    """
    easy_data = get_easy_data(data, paths)
    if easy_data.empty:
        return None
    easy_data = relevant_stops_to_data(easy_data, paths)
    return utils.apply_parallel(easy_data.groupby('linea'), get_s_d_para)


def get_bus_path(data, uid):
    """ Get the records for this uid. """
    return data[data.uid == uid]


def get_easy_data(data, paths):
    """ Get the part of the data that has lines which are in the paths dataframe. """
    known_lines = paths.linea.unique()
    return data[data.linea.isin(known_lines)]


def relevant_stops(paths, line):
    """ Get the bus stops, contained in the paths dataframe, that are in the line's path. """
    sub_paths = paths[paths.linea == line]
    stops = sub_paths.groupby('codigoParada').mean().drop('ordinal', axis=1)
    stops = stops.reset_index()
    return stops


def add_stop_cols(data, stops):
    """
    Creates columns for all the bus stops in 'stops', and merges them to the 'data' dataframe.
    All the values are filled with NaNs.
    """
    cols = ['d_' + str(stop) for stop in stops.codigoParada]
    distances = pd.DataFrame(np.full((data.shape[0], len(cols)), np.nan),
                             columns=cols,
                             index=data.index)
    return data.join(distances)


def relevant_stops_to_data(data, paths):
    """ Creates all the 'stops columns' that are present in any line that is contained in 'data'."""
    data_lines = data.linea.unique()
    sub_paths = paths[paths.linea.isin(data_lines)]
    stops = sub_paths.groupby('codigoParada').mean().drop('ordinal', axis=1)
    stops = stops.reset_index()
    return add_stop_cols(data, stops)


def get_stop_distances(data, paths, line):
    """ Given a dataframe that has all the stop-distances columns, it fills the relevant values. """
    # print('Getting distances for line {}'.format(line))
    stops = relevant_stops(paths, line)
    line_data = data[data.linea == line]
    for stop_idx in range(stops.shape[0]):
        stop = stops.iloc[stop_idx]
        line_data['d_' + str(int(stop.codigoParada))] = line_data[['x', 'y']].apply(
            lambda x: norm(x.values - stop[['x', 'y']]), axis=1)
    data.update(line_data)
    return data


def get_s_d_para(data):
    """ Auxiliary function to compute 'get_stop_distances in parallel'"""
    return get_stop_distances(data, paths=paths_s, line=data.linea.iloc[0])


def get_one_travel(data, uid, stop_id):
    """ Gets a dataframe with the distances of one uid to one bus stop. """
    return pd.DataFrame(data[data.uid == uid].set_index('timestamp').sort_index()[stop_id])


def time_list_mean(time_list):
    """ Calculate the mean of a list of Timestamps. """
    return pd.Timestamp.fromtimestamp(
        np.mean(list(map(lambda x: x.timestamp(), time_list))))


def get_one_ta(proximity):
    """
    Get the next time of arrival given a dataframe with booleans that represent that
    the bus is (or isn't) in the bus stop ('in_stop' column).
    """
    prox_t = proximity.reset_index()
    cluster_timestamps = list()
    ta = None
    last_in_stop = None
    for idx in range(prox_t.shape[0]):
        if prox_t.in_stop.iloc[idx]:
            for j in range(idx, prox_t.shape[0]):
                cluster_timestamps.append(prox_t.timestamp.iloc[j])
                if not prox_t.in_stop.iloc[j]:
                    break
            if prox_t.in_stop.iloc[j]:
                last_in_stop = j
            else:
                last_in_stop = j - 1
            break
    if len(cluster_timestamps) > 0:
        ta = time_list_mean(cluster_timestamps)
    return ta, last_in_stop


def fill_arrival_times(proximity, col_name, min_dist):
    """
    Receives a Series of booleans indexed in timestamps, and
    calculates the mean time of the "True" values clusters.
    It fills those values backwards.
    """
    new_col_name = col_name + '_ta'
    temp_prox = proximity.copy()
    temp_prox['in_stop'] = temp_prox[col_name] < min_dist
    temp_prox = temp_prox.sort_index()
    temp_prox[new_col_name] = np.full((temp_prox.shape[0],), np.nan)

    pre_last_in_stop = 0
    while pre_last_in_stop <= temp_prox.shape[0]:
        ta, last_in_stop = get_one_ta(temp_prox.iloc[pre_last_in_stop:, :])
        if ta is None:
            break
        start = temp_prox.index[pre_last_in_stop]
        end = temp_prox.index[pre_last_in_stop + last_in_stop]
        temp_prox.loc[start:end, new_col_name] = ta
        pre_last_in_stop += last_in_stop + 1
    return temp_prox[new_col_name]


def get_relevant_d_stops(paths, uid=None, line=None):
    """
    Pass a uid or a line, and it returns all the stop distances
    column names. uid and line cannot be both None.
    """
    if line is None:
        line = uid.split('_')[0]
    line_stops = relevant_stops(paths, line).codigoParada
    return line_stops.apply(lambda x: 'd_{}'.format(x))


def fill_one_uid(data, paths, uid, min_dist=50.0):
    """ Fills the times of arrival for one uid, and all the relevant bus stops. """
    temp_data = data.copy().set_index('timestamp')
    for col in get_relevant_d_stops(paths, uid=uid):
        new_col = col + '_ta'
        # print('\rFilling col {}'.format(new_col))
        if not (new_col in temp_data.columns):
            temp_data[new_col] = np.full((temp_data.shape[0],), pd.NaT)
        dists = pd.DataFrame(temp_data[col])
        temp_data.update(fill_arrival_times(dists, col, min_dist))
    ta_cols = temp_data.columns.str.contains('_ta')
    temp_data.loc[:, ta_cols] = temp_data.loc[:, ta_cols].apply(
        lambda x: pd.to_datetime(x))
    return temp_data


def fill_one_uid_para(uid_data):
    """ Auxiliary function to run fill_one_uid in parallel. """
    uid = uid_data.uid.iloc[0]
    tic = time()
    res = fill_one_uid(uid_data, paths_s, uid)
    toc = time()
    print('Filled uid: {} in {} seconds.'.format(uid, (toc-tic)))
    return res
