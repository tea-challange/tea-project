"""
Collection of functions to create a pandas DataFrame from
some simulated data, stored in raw_data.txt.
"""
import os
from src.data import DATA_RAW, ROOT_DIR, DATA_INTERIM, COEF_LAT, COEF_LONG, LATITUDE_MVD, LONGITUDE_MVD
from src.data import deg_to_plane
import datetime as dt
import pandas as pd
import json

DATA = os.path.join(DATA_RAW, 'raw_data.txt')


def parse_time(timestr):
    """ Converts a time string to datetime. Ignores the sub-second part. """
    return dt.datetime.strptime(timestr.split('.')[0], '%Y-%m-%dT%H:%M:%S')


def parse_line(line):
    """ Parses all the records for one timestamp. """
    record = json.loads(line.replace('\n', ''))['data'][0]

    # Regular data
    data = dict()
    data['id'] = record['id']
    data['timestamp'] = parse_time(record['timestamp']['value'])
    data['codigoBus'] = record['codigoBus']['value']
    data['linea'] = record['linea']['value']
    data['long'] = record['location']['value']['coordinates'][0]
    data['lat'] = record['location']['value']['coordinates'][1]
    data['uid'] = str(data['linea']) + '_' + str(data['codigoBus'])
    data['x'], data['y'] = deg_to_plane(data['long'], data['lat'])

    # GeoJson for fancy maps
    geographic = dict()
    geographic['timestamp'] = data['timestamp']
    geographic['linea'] = data['linea']
    geographic['codigoBus'] = data['codigoBus']
    geographic['geojson'] = record['location']

    return data, geographic


def parse_record(record):
    """ Parses one record. """
    # Regular data
    data = dict()
    data['id'] = record['id']
    data['timestamp'] = parse_time(record['timestamp']['value'])
    data['codigoBus'] = record['codigoBus']['value']
    data['linea'] = record['linea']['value']
    data['long'] = record['location']['value']['coordinates'][0]
    data['lat'] = record['location']['value']['coordinates'][1]
    data['uid'] = str(data['linea']) + '_' + str(data['codigoBus'])
    data['x'], data['y'] = deg_to_plane(data['long'], data['lat'])

    return data


def parse_file(filepath, parse_line_fun=parse_line):
    """ Returns a DataFrame with all the relevant data from a txt input file. """
    with open(filepath, 'r') as file:
        lines = file.readlines()
    data = list()
    geographic = list()
    for line in lines:
        data_line, geographic_line = parse_line_fun(line)
        data.append(data_line)
        geographic.append(geographic_line)
    return pd.DataFrame(data), pd.DataFrame(geographic)


def parse_data(root_dir=ROOT_DIR, save=False):
    """
    Parse the raw data to a DataFrame.
    """
    data_dir = os.path.join(root_dir, 'data')
    data_raw = os.path.join(data_dir, 'raw')
    raw_data_path = os.path.join(data_raw, 'raw_data.txt')
    data, geographic = parse_file(raw_data_path)
    data = data.drop_duplicates()
    data = data[~((data.long == 0) | (data.lat == 0))]  # Filter some zeros

    if save:
        data.to_pickle(os.path.join(DATA_INTERIM, 'sim_data.pkl'))
        data.to_csv(os.path.join(DATA_INTERIM, 'sim_data.csv'))
    return data, geographic
