"""
Collection of functions to create a pandas DataFrame from
the provided 'historia-buses' txt files.
"""
import datetime as dt
import json
import pandas as pd
import os
from src.data import ROOT_DIR, DATA_INTERIM, COEF_LAT, COEF_LONG, LATITUDE_MVD, LONGITUDE_MVD
from src.data import deg_to_plane


def parse_bus(bus):
    """ Parses one record. """
    bus_dict = dict()
    bus_dict['long'] = bus['geometry']['coordinates'][0]
    bus_dict['lat'] = bus['geometry']['coordinates'][1]
    bus_dict['codigoBus'] = bus['properties']['codigoBus']
    bus_dict['linea'] = bus['properties']['linea']
    bus_dict['uid'] = str(bus_dict['linea']) + '_' + str(bus_dict['codigoBus'])
    bus_dict['x'], bus_dict['y'] = deg_to_plane(bus_dict['long'], bus_dict['lat'])
    return bus_dict


def parse_time(timestr):
    """ Converts a time string to datetime. Ignores the sub-second part. """
    return dt.datetime.strptime(timestr.split('.')[0], '%Y-%m-%dT%H:%M:%S')


def parse_line(line):
    """ Parses all the records for one timestamp. """
    record = json.loads(line.replace('\n', ''))
    data = list()
    timestamp = parse_time(record['timestamp'])
    bus_records = record['posiciones']['features']
    for bus in bus_records:
        bus_dict = parse_bus(bus)
        bus_dict['timestamp'] = timestamp
        data.append(bus_dict)
    return data


def parse_file(filepath, parse_line_fun=parse_line):
    """ Returns a DataFrame with all the relevant data from a txt input file. """
    with open(filepath, 'r') as file:
        lines = file.readlines()
    data = list()
    for line in lines:
        data += parse_line_fun(line)
    return pd.DataFrame(data)


def parse_data(root_dir=ROOT_DIR, concat=True, save=False):
    """
    If concat = False, returns a list of DataFrames, one for each txt input file in the
    RAW dir.
    If concat=True, it returns a concatenated DataFrame with all the data.
    """
    data_dir = os.path.join(root_dir, 'data')
    data_raw = os.path.join(data_dir, 'raw')
    txt_paths = ['historia-buses.txt'] + ['historia-buses{}.txt'.format(i) for i in range(1, 4)]
    txt_paths = [os.path.join(data_raw, name) for name in txt_paths]
    data = list()
    for path in txt_paths:
        data_i = parse_file(path)
        data_i = data_i[~((data_i.long == 0) | (data_i.lat == 0))]  # Filter some zeros
        data.append(data_i)

    if concat:
        data_df = pd.concat(data, ignore_index=True)
        if save:
            data_df.to_pickle(os.path.join(DATA_INTERIM, 'data.pkl'))
            data_df.to_csv(os.path.join(DATA_INTERIM, 'data.csv'))
        return data_df

    return data
