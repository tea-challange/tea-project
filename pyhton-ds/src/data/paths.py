""" Functions related to the trayectosporlinea.json file. """
import pandas as pd
from src.data import PATHS_JSON, DATA_PROCESSED, PATHS_CSV
from src.data import deg_to_plane
import os


def get_paths(path_to_file=PATHS_JSON, save=False):
    """ Loads and parses the file 'trayectosporlinea.json' """
    paths = pd.read_json(path_to_file)
    paths = pd.DataFrame(paths.values.flatten().tolist()).dropna()
    paths = paths.astype({'codigoParada': int,
                          'lat': float,
                          'long': float,
                          'ordinal': int})  # linea may not be an integer (e.g.: D11)
    paths['x'], paths['y'] = deg_to_plane(paths['long'], paths['lat'])
    if save:
        paths.to_pickle(os.path.join(DATA_PROCESSED, 'paths.pkl'))
    return paths


def get_paths_csv(path_to_file=PATHS_CSV, save=False):
    """ Loads and parses the file 'trayectosporlinea.csv' """
    cols = ['codigoParada',
            'linea',
            'ordinal',
            'calle',
            'esquina',
            'long',
            'lat']
    paths = pd.read_csv(PATHS_CSV, delimiter='|', header=None)
    paths = paths.rename(columns={old: new for old, new in zip(paths.columns, cols)})
    paths = paths.astype({'codigoParada': int,
                          'linea': str,
                          'calle': str,
                          'esquina': str,
                          'lat': float,
                          'long': float,
                          'ordinal': int})  # linea may not be an integer (e.g.: D11)
    paths['x'], paths['y'] = deg_to_plane(paths['long'], paths['lat'])
    if save:
        paths.to_pickle(os.path.join(DATA_PROCESSED, 'paths_final.pkl'))
    return paths



def get_line_path(paths, line):
    """ Gets the complete path for a given line. """
    return paths[paths.linea == str(line)]
