import os
from pathlib import Path
import math

MODULE_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = str(Path(MODULE_DIR).parent.parent)
DATA_DIR = os.path.join(ROOT_DIR, 'data')
DATA_RAW = os.path.join(DATA_DIR, 'raw')
DATA_INTERIM = os.path.join(DATA_DIR, 'interim')
DATA_EXTERNAL = os.path.join(DATA_DIR, 'external')
DATA_PROCESSED = os.path.join(DATA_DIR, 'processed')
PATHS_JSON = os.path.join(DATA_RAW, 'trayectosporlinea.json')

DATA = os.path.join(DATA_INTERIM, 'data.pkl')
PATHS = os.path.join(DATA_PROCESSED, 'paths.pkl')
SIM_DATA = os.path.join(DATA_INTERIM, 'sim_data.pkl')
DISTS_TAS = os.path.join(DATA_INTERIM, 'dists_and_tas.pkl')
TOTAL_DATA = os.path.join(DATA_INTERIM, 'total_data.pkl')

X_TRAIN = os.path.join(DATA_PROCESSED, 'X_train.pkl')
X_VAL = os.path.join(DATA_PROCESSED, 'X_val.pkl')
X_TEST = os.path.join(DATA_PROCESSED, 'X_test.pkl')
Y_TRAIN = os.path.join(DATA_PROCESSED, 'y_train.pkl')
Y_VAL = os.path.join(DATA_PROCESSED, 'y_val.pkl')
Y_TEST = os.path.join(DATA_PROCESSED, 'y_test.pkl')

X_VAL_RAW = os.path.join(DATA_PROCESSED, 'X_val_raw.pkl')
X_TEST_RAW = os.path.join(DATA_PROCESSED, 'X_test_raw.pkl')

VAL_SET = os.path.join(DATA_PROCESSED, 'eval_set.pkl')
TEST_SET = os.path.join(DATA_PROCESSED, 'test_set.pkl')

STOPS_MEAN_TAS = os.path.join(DATA_PROCESSED, 'stops_mean_tas.pkl')

X_TRAIN_FINAL = os.path.join(DATA_PROCESSED, 'X_train_final.pkl')
Y_TRAIN_FINAL = os.path.join(DATA_PROCESSED, 'y_train_final.pkl')
X_VAL_FINAL = os.path.join(DATA_PROCESSED, 'X_val_final.pkl')
Y_VAL_FINAL = os.path.join(DATA_PROCESSED, 'y_val_final.pkl')
X_DATA_FINAL = os.path.join(DATA_PROCESSED, 'X_data_final.pkl')
Y_DATA_FINAL = os.path.join(DATA_PROCESSED, 'y_data_final.pkl')

PATHS_CSV = os.path.join(DATA_RAW, 'trayectosporlinea_v2.csv')

EARTH_RADIUS = 6371000.0  # In meters
LATITUDE_MVD = -34.90328
LONGITUDE_MVD = -56.18816

COEF_LONG = math.fabs(EARTH_RADIUS * math.cos(LATITUDE_MVD)) * math.pi / 180.0
COEF_LAT = EARTH_RADIUS * math.pi / 180.0


def deg_to_plane(long, lat):
    """ Converts the lat/long coordinates to plane distances (in meters). """
    x = COEF_LONG * (long - LONGITUDE_MVD)
    y = COEF_LAT * (lat - LATITUDE_MVD)
    return x, y
