package uy.com.earlgrey.ngsireceiver.utils;

import uy.com.earlgrey.ngsireceiver.dto.Point;

/**
 * Created by eviotti on  21/10/18.
 */
public class GPSUtils {

    private static final double EARTH_RADIUS = 6371000;
    //    private static final double EARTH_RADIUS = 6378137;
    private static final Point CENTER = new Point(-34.906582, -56.203530);

    public static Point projectTo2NPlane2(double latitude, double longitude) {
//            x = R * cos(lat) * cos(lon)
//            y = R * cos(lat) * sin(lon)

        double latRadians = Math.toRadians(latitude);
        double longRadians = Math.toRadians(longitude);
        double x = EARTH_RADIUS * Math.cos(latRadians) * Math.cos(longRadians);
        double y = EARTH_RADIUS * Math.cos(latRadians) * Math.sin(longRadians);

        return new Point(x, y);
    }

    public static Point projectTo2NPlane(double latitude, double longitude) {
//        x = Longitude ∙ R cos(Latitude)
//        y = Latitude ∙ R
        double x = longitude * EARTH_RADIUS * Math.cos(CENTER.getX());
        double y = latitude * EARTH_RADIUS;

        return new Point(x, y);
    }
}
