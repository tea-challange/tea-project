package uy.com.earlgrey.ngsireceiver.dto;

/**
 * Created by eviotti on  21/10/18.
 */
public class Distance {
    private double value;
    private String method;

    public Distance(double value, String method) {
        this.value = value;
        this.method = method;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Distance distance = (Distance) o;

        if (Double.compare(distance.value, value) != 0) return false;
        return method != null ? method.equals(distance.method) : distance.method == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(value);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (method != null ? method.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Distance{" +
                "value=" + value +
                ", method='" + method + '\'' +
                '}';
    }
}
