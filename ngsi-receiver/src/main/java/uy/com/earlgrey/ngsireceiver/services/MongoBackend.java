package uy.com.earlgrey.ngsireceiver.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uy.com.earlgrey.ngsireceiver.config.ConfigController;
import uy.com.earlgrey.ngsireceiver.controllers.NGSIV2Controller;
import uy.com.earlgrey.ngsireceiver.dto.LineRoute;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by eviotti on  12/10/18.
 */

public class MongoBackend {

    private Logger logger = LoggerFactory.getLogger(NGSIV2Controller.class);

    public MongoBackend() {
    }

    @Deprecated
    public void saveDocument(JsonNode document) {

        MongoClient mongoClient = null;
        try {

            ConfigController config = new ConfigController();

            mongoClient = new MongoClient(config.getMongoHost(), config.getMongoPort());

            MongoDatabase database = mongoClient.getDatabase("buses");
            MongoCollection<Document> collection = database.getCollection("events");
            collection.insertOne(jsonNodeToDocument(document));

        } catch (JsonProcessingException ex) {
            logger.error("Error al convertir JSONNode a Document {}", document);
        } catch (Exception ex) {
            logger.error("Error al conectarse con la BD");
        } finally {
            if (mongoClient != null) {
                mongoClient.close();
            }
        }
    }

    public void saveDocuments(JsonNode colDocuments) {

        MongoClient mongoClient = null;
        try {

            ConfigController config = new ConfigController();
            mongoClient = new MongoClient(config.getMongoHost(), config.getMongoPort());

            MongoDatabase database = mongoClient.getDatabase("buses");
            MongoCollection<Document> collection = database.getCollection("events2");

            List<Document> toInsert = new ArrayList<Document>();
            if (colDocuments.isArray()) {
                for (final JsonNode objNode : colDocuments) {
                    try {
                        toInsert.add(jsonNodeToDocument(objNode));
                    } catch (JsonProcessingException ex) {
                        logger.error("Error al convertir JSONNode a Document {}", objNode);
                    }
                }
            }

            collection.insertMany(toInsert);

        } catch (Exception ex) {
            logger.error("Error al conectarse con la BD");
        } finally {
            if (mongoClient != null) {
                mongoClient.close();
            }
        }
    }

    private Document jsonNodeToDocument(JsonNode jsonNode) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.treeToValue(jsonNode, Document.class);
    }

    private JsonNode documentToJsonNode(Document document) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.valueToTree(document);
    }

    public void saveBusRoutes(JsonNode colDocuments) {
        logger.info("======> MongoBackend :: saveBusRoutes ");

        MongoClient mongoClient = null;
        try {

            ConfigController config = new ConfigController();
            mongoClient = new MongoClient(config.getMongoHost(), config.getMongoPort());

            MongoDatabase database = mongoClient.getDatabase("buses");
            MongoCollection<Document> collection = database.getCollection("trayectosporlinea");

            List<Document> toInsert = new ArrayList<Document>();
            if (colDocuments.isArray()) {
                for (final JsonNode objNode : colDocuments) {
                    try {
                        toInsert.add(jsonNodeToDocument(objNode));
                    } catch (JsonProcessingException ex) {
                        logger.error("Error al convertir JSONNode a Document {}", objNode);
                    }
                }
            }

            collection.insertMany(toInsert);

        } catch (Exception e) {
            logger.error("Error al conectarse con la BD {}", e);
        } finally {
            if (mongoClient != null) {
                mongoClient.close();
            }
        }
    }

    public void saveBusRoutesObject(Map<String, LineRoute> routes) {
        logger.info("======> MongoBackend :: saveBusRoutesObject ");

        MongoClient mongoClient = null;
        try {

            ConfigController config = new ConfigController();
            mongoClient = new MongoClient(config.getMongoHost(), config.getMongoPort());

            MongoDatabase database = mongoClient.getDatabase("buses");
            MongoCollection<Document> collection = database.getCollection("trayectosporlineaProcessed");

            List<Document> toInsert = new ArrayList<Document>();
            ObjectMapper mapper = new ObjectMapper();

            for ( Map.Entry<String, LineRoute> entry : routes.entrySet() ) {
                String line = entry.getKey();
                LineRoute route = entry.getValue();
                toInsert.add(Document.parse(mapper.writeValueAsString(route)));
            }

            collection.insertMany(toInsert);

        } catch (Exception e) {
            logger.error("Error al conectarse con la BD {}", e);
        } finally {
            if (mongoClient != null) {
                mongoClient.close();
            }
        }
    }

    public void cleanBusRoutesCollection() {
        logger.info("======> MongoBackend :: cleanBusRoutesCollection ");

        MongoClient mongoClient = null;
        try {

            ConfigController config = new ConfigController();
            mongoClient = new MongoClient(config.getMongoHost(), config.getMongoPort());

            MongoDatabase database = mongoClient.getDatabase("buses");
            MongoCollection<Document> collection = database.getCollection("trayectosporlinea");

            collection.deleteMany(new Document());

        } catch (Exception e) {
            logger.error("Error al conectarse con la BD {}", e);
        } finally {
            if (mongoClient != null) {
                mongoClient.close();
            }
        }
    }

    public void cleanBusPreProcessedRoutesCollection() {
        logger.info("======> MongoBackend :: cleanBusRoutesCollection ");

        MongoClient mongoClient = null;
        try {

            ConfigController config = new ConfigController();
            mongoClient = new MongoClient(config.getMongoHost(), config.getMongoPort());

            MongoDatabase database = mongoClient.getDatabase("buses");
            MongoCollection<Document> collection = database.getCollection("trayectosporlineaProcessed");

            collection.deleteMany(new Document());

        } catch (Exception e) {
            logger.error("Error al conectarse con la BD {}", e);
        } finally {
            if (mongoClient != null) {
                mongoClient.close();
            }
        }
    }

    public JsonNode getBusRoutes() {

        MongoClient mongoClient = null;
        MongoCursor<Document> cursor = null;
        ArrayList<JsonNode> routes = new ArrayList<JsonNode>();

        try {

            ConfigController config = new ConfigController();
            mongoClient = new MongoClient(config.getMongoHost(), config.getMongoPort());

            MongoDatabase database = mongoClient.getDatabase("buses");
            MongoCollection<Document> collection = database.getCollection("trayectosporlinea");

            FindIterable<Document> result = collection.find();
            cursor = result.iterator();
            while (cursor.hasNext()) {
                routes.add(documentToJsonNode(cursor.next()));
            }

        } catch (Exception ex) {
            logger.error("Error al conectarse con la BD");
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            if (mongoClient != null) {
                mongoClient.close();
            }
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.createArrayNode().addAll(routes);
    }
}
