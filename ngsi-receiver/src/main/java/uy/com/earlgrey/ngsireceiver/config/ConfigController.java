package uy.com.earlgrey.ngsireceiver.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uy.com.earlgrey.ngsireceiver.controllers.NGSIV2Controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by eviotti on  13/10/18.
 */

public class ConfigController {

    // Constantes
    private final static String ORION_URL_ENV_NAME = "ORION_URL";
    private final static String SIMULADOR_URL_ENV_NAME = "SIMULADOR_URL";
    private final static String TEA_CORE_URL_ENV_NAME = "TEA_CORE_URL";
    private final static String NGSI_RECEIVER_URL_ENV_NAME = "SERVICIO_URL";
    private final static String MONGO_HOST_ENV_NAME = "MONGO_HOST";
    private final static String MONGO_PORT_ENV_NAME = "MONGO_PORT";

    private Logger logger = LoggerFactory.getLogger(NGSIV2Controller.class);

    private String orionUrl;
    private String simuladorUrl;
    private String teaCoreUrl;
    private String ngsiReceiverUrl;
    private String mongoHost;
    private int mongoPort;

    private Properties properties = new Properties();

    public ConfigController() {
        InputStream input = null;

        try {

            final String filename = "application.properties";
            input = ConfigController.class.getClassLoader().getResourceAsStream(filename);
            if (input == null) {
                logger.error("Error loading configuration file {}", filename);
            }

            //load a properties file from class path, inside static method
            properties.load(input);

            // ORION URL
            if (System.getenv(ORION_URL_ENV_NAME) != null) {
                orionUrl = System.getenv(ORION_URL_ENV_NAME);
            } else {
                orionUrl = properties.getProperty("uy.com.earlgrey.ngsireceiver.orionUrl");
            }

            // SIMULADOR URL
            if (System.getenv(SIMULADOR_URL_ENV_NAME) != null) {
                simuladorUrl = System.getenv(SIMULADOR_URL_ENV_NAME);
            } else {
                simuladorUrl = properties.getProperty("uy.com.earlgrey.ngsireceiver.simuladorUrl");
            }

            // TEA CORE URL
            if (System.getenv(TEA_CORE_URL_ENV_NAME) != null) {
                teaCoreUrl = System.getenv(TEA_CORE_URL_ENV_NAME);
            } else {
                teaCoreUrl = properties.getProperty("uy.com.earlgrey.ngsireceiver.teaCoreUrl");
            }

            // NGSI RECEIVER URL
            if (System.getenv(NGSI_RECEIVER_URL_ENV_NAME) != null) {
                ngsiReceiverUrl = System.getenv(NGSI_RECEIVER_URL_ENV_NAME);
            } else {
                ngsiReceiverUrl = properties.getProperty("uy.com.earlgrey.ngsireceiver.Url");
            }

            // Mongo HOST
            if (System.getenv(MONGO_HOST_ENV_NAME) != null) {
                mongoHost = System.getenv(MONGO_HOST_ENV_NAME);
            } else {
                mongoHost = properties.getProperty("uy.com.earlgrey.ngsireceiver.mongo.host");
            }

            // Mongo PORT
            if (System.getenv(MONGO_PORT_ENV_NAME) != null) {
                mongoPort = Integer.parseInt(System.getenv(MONGO_PORT_ENV_NAME));
            } else {
                mongoPort = Integer.parseInt(properties.getProperty("uy.com.earlgrey.ngsireceiver.mongo.port"));
            }

        } catch (IOException ex) {
            logger.error("Error loading configuration file ");
        }
    }

    public String getOrionUrl() {
        return orionUrl;
    }

    public String getSimuladorUrl() {
        return simuladorUrl;
    }

    public String getTeaCoreUrl() {
        return teaCoreUrl;
    }

    public String getNgsiReceiverUrl() {
        return ngsiReceiverUrl;
    }

    public String getMongoHost() {
        return mongoHost;
    }

    public int getMongoPort() {
        return mongoPort;
    }

    @Override
    public String toString() {
        return "ConfigController{" +
                "orionUrl='" + orionUrl + '\'' +
                ", simuladorUrl='" + simuladorUrl + '\'' +
                ", teaCoreUrl='" + teaCoreUrl + '\'' +
                ", ngsiReceiverUrl='" + ngsiReceiverUrl + '\'' +
                ", mongoHost='" + mongoHost + '\'' +
                ", mongoPort=" + mongoPort +
                '}';
    }
}
