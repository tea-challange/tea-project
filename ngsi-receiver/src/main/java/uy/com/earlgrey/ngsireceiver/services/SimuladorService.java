package uy.com.earlgrey.ngsireceiver.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.ClientProtocolException;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uy.com.earlgrey.ngsireceiver.config.ConfigController;
import uy.com.earlgrey.ngsireceiver.controllers.NGSIV2Controller;

/**
 * Created by eviotti on  13/10/18.
 */
public class SimuladorService {

    private Logger logger = LoggerFactory.getLogger(NGSIV2Controller.class);

    public JsonNode loadBusRoutes() throws Exception {
        logger.info("======> SimuladorService: loadBusRoutes ");

        ConfigController config = new ConfigController();
        String simuladorURL = config.getSimuladorUrl();

        String uri = simuladorURL + "/api/trayectosporlinea";

        try {
            ClientRequest request = new ClientRequest(uri);
            request.accept("application/json");
            ClientResponse<String> response = request.get(String.class);

            if (response.getStatus() != 200) {
                logger.error("Error connecting to simulador {}", response);
            }

            ObjectMapper mapper = new ObjectMapper();
            return  mapper.readTree(response.getEntity());

        } catch (ClientProtocolException e) {
            logger.error("Error connecting to simulador {}", e);
            throw e; //TODO: refinar a una exception de la aplicacion
        } catch (Exception e) {
            logger.error("Error connecting to simulador {}", e);
            throw e; //TODO: refinar a una exception de la aplicacion
        }
    }

}
