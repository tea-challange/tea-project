package uy.com.earlgrey.ngsireceiver.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uy.com.earlgrey.ngsireceiver.config.ConfigController;
import uy.com.earlgrey.ngsireceiver.controllers.NGSIV2Controller;
import uy.com.earlgrey.ngsireceiver.controllers.UtilsController;
import uy.com.earlgrey.ngsireceiver.services.OrionService;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/ngsireceiver")
public class NGSIReceiverApplication extends Application {
	private Set<Object> singletons = new HashSet<Object>();

    private Logger logger = LoggerFactory.getLogger(NGSIV2Controller.class);

    public NGSIReceiverApplication() {
		// Register services
		singletons.add(new NGSIV2Controller());
		singletons.add(new UtilsController());

        // Logs actual loaded configuration
        ConfigController config = new ConfigController();
        logger.info("CONFIGURATION \n" + config.toString());

        OrionService orionService = new OrionService();
        orionService.createSubscription();
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
