package uy.com.earlgrey.ngsireceiver.services;

import com.fasterxml.jackson.core.JsonParseException;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uy.com.earlgrey.ngsireceiver.config.ConfigController;
import uy.com.earlgrey.ngsireceiver.controllers.NGSIV2Controller;

/**
 * Created by eviotti on  25/10/18.
 */
public class OrionService {

    private Logger logger = LoggerFactory.getLogger(NGSIV2Controller.class);

    public void createSubscription() {
        logger.info("======> OrionService: createSubscription ");

        ConfigController config = new ConfigController();
        String orionURL = config.getOrionUrl();

        String uri = orionURL + "/v2/subscriptions";

        try {

            String subscriptionString = "{" +
                    "\"subject\": {" +
                    "\"entities\": [ " +
                    "{ " +
                    "\"idPattern\": \".*\"," +
                    "\"type\": \"Bus\"" +
                    "}" +
                    "]," +
                    "\"condition\": { \"attrs\": []}" +
                    "}, " +
                    "\"notification\": { \"http\": { \"url\": \"%s\"}}" +
                    "}";

            String meUrl = config.getNgsiReceiverUrl() + "/ngsireceiver/v2/notify";
            String body = String.format(subscriptionString, meUrl);
            logger.info(body);

            ClientRequest request = new ClientRequest(uri);
            request.accept("application/json");
            request.body("application/json", body);
            ClientResponse<String> response = request.post(String.class);

            if (response.getStatus() != 201) {
                logger.error("Error connecting to Orion Context Broker. Status={} Msg={}",
                        response.getStatusInfo().toString(), response.getEntity());
            }

        } catch (JsonParseException e) {
            logger.error("Error creating subscription request {}", e);
        } catch (Exception e) {
            logger.error("Error connecting to Orion Context Broker {}", e);
            logger.error("Error connecting to Orion Context Broker {}", e.getMessage());
        }
    }
}
