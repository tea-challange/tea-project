package uy.com.earlgrey.ngsireceiver.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uy.com.earlgrey.ngsireceiver.dto.LineRoute;
import uy.com.earlgrey.ngsireceiver.services.BusRoutesPreProcessor;
import uy.com.earlgrey.ngsireceiver.services.MongoBackend;
import uy.com.earlgrey.ngsireceiver.services.SimuladorService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * Created by eviotti on  13/10/18.
 */
@Path("/utils")
public class UtilsController {

    private Logger logger = LoggerFactory.getLogger(NGSIV2Controller.class);

    public UtilsController() {
    }

    @GET
    @Produces("application/json")
    @Path("/trayectosporlinea")
    public Response getBusRoutes(@QueryParam("operation") String operation) {
        logger.info("======> Utils trayectos por linea ");

        try {
            JsonNode busRoutes = null;
            if (operation == null || operation.equals("load")) {
                logger.info("======> Cargando trayectos ");
                SimuladorService simuladorService = new SimuladorService();
                JsonNode simulatorResult = simuladorService.loadBusRoutes();
                if (simulatorResult != null) {
                    JsonNode data = simulatorResult.get("trayectos");
                    if (data != null) {

                        // New
                        BusRoutesPreProcessor processor = new BusRoutesPreProcessor();
                        Map<String, LineRoute> preProcessedData = processor.processData(data);
                        logger.info("======> termine ");
                        //

                        MongoBackend mongoBackend = new MongoBackend();
                        // Clean collection after save
                        mongoBackend.cleanBusRoutesCollection();
                        mongoBackend.cleanBusPreProcessedRoutesCollection();
                        // Save original routes
                        mongoBackend.saveBusRoutes(data);
                        // Save pre processed routes
                        mongoBackend.saveBusRoutesObject(preProcessedData);

                        busRoutes = data;
                    }
                }

            } else {
                logger.info("======> Recuperando trayectos desde BD ");
                MongoBackend mongoBackend = new MongoBackend();
                busRoutes = mongoBackend.getBusRoutes();
            }

            return Response.status(200).entity(busRoutes).build();

        } catch (Exception e) {
            return Response.status(500).entity(e.toString()).build();
        }

    }
}
