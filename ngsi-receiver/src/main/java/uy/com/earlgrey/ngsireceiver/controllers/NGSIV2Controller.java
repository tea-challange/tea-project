package uy.com.earlgrey.ngsireceiver.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uy.com.earlgrey.ngsireceiver.services.MongoBackend;
import uy.com.earlgrey.ngsireceiver.services.TeaCoreService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by eviotti on  10/10/18.
 */
@Path("/v2")
public class NGSIV2Controller {

    private Logger logger = LoggerFactory.getLogger(NGSIV2Controller.class);

    private MongoBackend mongoBackend;

    public NGSIV2Controller() {
        mongoBackend = new MongoBackend();
    }

    @POST
    @Consumes("application/json")
    @Path("/notify")
    public Response receiveNotification(String notification) {

        logger.info("======> RECEIVE NEW DATA ");
        logger.debug("{}", notification);

        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode obj = mapper.readTree(notification);
            JsonNode data = obj.get("data");
            if (data != null) {
                mongoBackend.saveDocuments(data);
            }

            TeaCoreService core = new TeaCoreService();

            // Pueden existir más de un updateContext Notify
            if (obj.isArray()) {
                for (final JsonNode n : obj) {
                    core.sendNotificationEvent(n);
                }
            }

        } catch (IOException e) {
            logger.error("Error parsing notification to JSON {}", notification);
        }

        return Response.status(200).entity(notification).build();

    }

    @GET
    @Path("/status")
    public Response ping() {
        logger.info("status");
        return Response.status(200).entity("ok").build();

    }
}
