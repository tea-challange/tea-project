package uy.com.earlgrey.ngsireceiver.services;

import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uy.com.earlgrey.ngsireceiver.dto.Distance;
import uy.com.earlgrey.ngsireceiver.dto.LineRoute;
import uy.com.earlgrey.ngsireceiver.dto.LineStop;
import uy.com.earlgrey.ngsireceiver.dto.Point;
import uy.com.earlgrey.ngsireceiver.utils.Distances;
import uy.com.earlgrey.ngsireceiver.utils.GPSUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by eviotti on  21/10/18.
 */
public class BusRoutesPreProcessor {

    private Logger logger = LoggerFactory.getLogger(BusRoutesPreProcessor.class);

    public Map<String, LineRoute> processData(JsonNode data) {
        logger.info("======> processData ");

        // Put all data into dictionary by linea as a key
        Map<String, LineRoute> lines = new HashMap<String, LineRoute>();

        if (data.isArray()) {

            for (final JsonNode objNode : data) {

                LineRoute route;
                if (objNode.has("linea")) {
                    String line = objNode.get("linea").asText();
                    if (!lines.containsKey(line)) {
                        route = new LineRoute(line);
                        lines.put(line, route);
                    } else {
                        route = lines.get(line);
                    }

                    LineStop stop = new LineStop();
                    stop.setCodigoParada(objNode.has("codigoParada") ? objNode.get("codigoParada").asText() : "");
                    stop.setOrdinal(objNode.has("ordinal") ? Integer.parseInt(objNode.get("ordinal").asText()) : 0);
                    stop.setCalle(objNode.has("calle") ? objNode.get("calle").asText() : "");
                    stop.setEsquina(objNode.has("esquina") ? objNode.get("esquina").asText() : "");
                    stop.setLatitude(objNode.has("lat") ? Double.parseDouble(objNode.get("lat").asText()) : 0);
                    stop.setLongitude(objNode.has("long") ? Double.parseDouble(objNode.get("long").asText()) : 0);

                    // Flat coordinates
                    Point p = GPSUtils.projectTo2NPlane2(stop.getLatitude(), stop.getLongitude());
                    stop.setProjecttionTo2NPlane(p);

                    // Add new stop
                    route.addStop(stop);
                }
            }

            for (Map.Entry<String, LineRoute> entry : lines.entrySet() ) {
                LineRoute route = entry.getValue();
                Optional<LineStop> originStop = route.getStops().stream().filter(s -> s.getOrdinal() == 1).findFirst();
                if(originStop.isPresent()){
                    route.setOrigin(originStop.get());

                    for(LineStop stop : route.getStops()){

                        double distanceToOrigin = Distances.euclideanDistancePoint(originStop.get().getProjecttionTo2NPlane(), stop.getProjecttionTo2NPlane());
                        Distance d = new Distance(distanceToOrigin, "euclideanDistancePoint");
                        stop.addDistance(d);
                    }
                }
            }
        }

        return lines;
    }
}
