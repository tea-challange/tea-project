package uy.com.earlgrey.ngsireceiver.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eviotti on  21/10/18.
 */
public class LineRoute {
    private String line;
    private LineStop origin;
    private List<LineStop> stops;

    public LineRoute() {
    }

    public LineRoute(String line) {
        this.line = line;
        this.stops = new ArrayList<LineStop>();
    }

    public LineRoute(String line, List<LineStop> stops) {
        this.line = line;
        this.stops = stops;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public List<LineStop> getStops() {
        return stops;
    }

    public void setStops(List<LineStop> stops) {
        this.stops = stops;
    }

    public void addStop(LineStop stop) {
        this.stops.add(stop);
    }

    public LineStop getOrigin() {
        return origin;
    }

    public void setOrigin(LineStop origin) {
        this.origin = origin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LineRoute lineRoute = (LineRoute) o;

        if (line != null ? !line.equals(lineRoute.line) : lineRoute.line != null) return false;
        if (origin != null ? !origin.equals(lineRoute.origin) : lineRoute.origin != null) return false;
        return stops != null ? stops.equals(lineRoute.stops) : lineRoute.stops == null;
    }

    @Override
    public int hashCode() {
        int result = line != null ? line.hashCode() : 0;
        result = 31 * result + (origin != null ? origin.hashCode() : 0);
        result = 31 * result + (stops != null ? stops.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LineRoute{" +
                "line='" + line + '\'' +
                ", origin=" + origin +
                ", stops=" + stops +
                '}';
    }
}
