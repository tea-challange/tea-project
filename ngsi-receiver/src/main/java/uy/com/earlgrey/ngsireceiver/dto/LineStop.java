package uy.com.earlgrey.ngsireceiver.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eviotti on  21/10/18.
 */
public class LineStop {
    private String codigoParada;
    private int ordinal;
    private String calle;
    private String esquina;
    private double latitude;
    private double longitude;
    private Point projecttionTo2NPlane;
    private List<Distance> distances = new ArrayList<>();

    public LineStop() {
    }

    public LineStop(String codigoParada, int ordinal, String calle, String esquina, double latitude, double longitude, Point projecttionTo2NPlane, List<Distance> distances) {
        this.codigoParada = codigoParada;
        this.ordinal = ordinal;
        this.calle = calle;
        this.esquina = esquina;
        this.latitude = latitude;
        this.longitude = longitude;
        this.projecttionTo2NPlane = projecttionTo2NPlane;
        this.distances = distances;
    }

    public String getCodigoParada() {
        return codigoParada;
    }

    public void setCodigoParada(String codigoParada) {
        this.codigoParada = codigoParada;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getEsquina() {
        return esquina;
    }

    public void setEsquina(String esquina) {
        this.esquina = esquina;
    }

    public double getLatitude() {
        return latitude;
    }


    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    public Point getProjecttionTo2NPlane() {
        return projecttionTo2NPlane;
    }

    public void setProjecttionTo2NPlane(Point projecttionTo2NPlane) {
        this.projecttionTo2NPlane = projecttionTo2NPlane;
    }

    public List<Distance> getDistances() {
        return distances;
    }

    public void setDistances(List<Distance> distances) {
        this.distances = distances;
    }

    public void addDistance(Distance d){
        this.distances.add(d);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LineStop lineStop = (LineStop) o;

        if (ordinal != lineStop.ordinal) return false;
        if (latitude != lineStop.latitude) return false;
        if (longitude != lineStop.longitude) return false;
        if (codigoParada != null ? !codigoParada.equals(lineStop.codigoParada) : lineStop.codigoParada != null)
            return false;
        if (calle != null ? !calle.equals(lineStop.calle) : lineStop.calle != null) return false;
        if (esquina != null ? !esquina.equals(lineStop.esquina) : lineStop.esquina != null) return false;
        if (projecttionTo2NPlane != null ? !projecttionTo2NPlane.equals(lineStop.projecttionTo2NPlane) : lineStop.projecttionTo2NPlane != null)
            return false;
        return distances != null ? distances.equals(lineStop.distances) : lineStop.distances == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = codigoParada != null ? codigoParada.hashCode() : 0;
        result = 31 * result + ordinal;
        result = 31 * result + (calle != null ? calle.hashCode() : 0);
        result = 31 * result + (esquina != null ? esquina.hashCode() : 0);
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (projecttionTo2NPlane != null ? projecttionTo2NPlane.hashCode() : 0);
        result = 31 * result + (distances != null ? distances.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LineStop{" +
                "codigoParada='" + codigoParada + '\'' +
                ", ordinal=" + ordinal +
                ", calle='" + calle + '\'' +
                ", esquina='" + esquina + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", projecttionTo2NPlane=" + projecttionTo2NPlane +
                ", distances=" + distances +
                '}';
    }
}
