package uy.com.earlgrey.ngsireceiver.services;

import com.fasterxml.jackson.databind.JsonNode;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uy.com.earlgrey.ngsireceiver.config.ConfigController;
import uy.com.earlgrey.ngsireceiver.controllers.NGSIV2Controller;

/**
 * Created by eviotti on  24/10/18.
 */
public class TeaCoreService {

    private Logger logger = LoggerFactory.getLogger(NGSIV2Controller.class);

    public void sendNotificationEvent(JsonNode data) {
        logger.info("======> TeaCoreService: sendNotificationEvent ");

        ConfigController config = new ConfigController();
        String teaCoreURL = config.getTeaCoreUrl();

        String uri = teaCoreURL + "/update";

        try {

            ClientRequest request = new ClientRequest(uri);
            request.accept("application/json");
            request.body("application/json", data);
            ClientResponse<String> response = request.post(String.class);

            if (response.getStatus() != 200) {
                logger.error("Error connecting to simulador {}", response);
            }

        } catch (Exception e) {
            logger.error("Error connecting to TEA Core {}", e);
        }
    }
}
