package uy.com.earlgrey.ngsireceiver.utils;

import uy.com.earlgrey.ngsireceiver.dto.LocationDto;
import uy.com.earlgrey.ngsireceiver.dto.Point;

/**
 * Created by eviotti on  21/10/18.
 */
public class Distances {

    public static double euclideanDistance(LocationDto a, LocationDto b){
        return Math.sqrt(Math.pow(b.getCoordinates().get(0) - a.getCoordinates().get(0), 2) +
                Math.pow(b.getCoordinates().get(1) - a.getCoordinates().get(1), 2));
    }

    public static double euclideanDistancePoint(Point a, Point b){
        return Math.sqrt(Math.pow(b.getX() - a.getX(), 2) +
                Math.pow(b.getY() - a.getY(), 2));
    }
}
