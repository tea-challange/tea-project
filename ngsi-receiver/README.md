# NGSI RECEIVER

## Sobre el proyecto
Este proyecto expone una API REST de para facilitar la comunicación entre  la componente
Orion Context Broker de FIWARE y el resto de las componentes desarrolladas para resolver el 
desaío TEA. 

El proyecto esta desarrollado con Java 8 y los servicios se encuentran implementados con jax-rs. En particular a travéz de la 
libreria RestEasy.

## Servicios
Se listan los servicios disponibles

#### /ngsireceiver/status
Devuelve el status de la aplicación: "OK" si esta todo bien.

Ejemplo:
```
GET http://[HOST]:[PUERTO]/ngsireceiver/status
```

#### /ngsireceiver/v2/notify
Endpoint para recibir notificaciones de Orion Context Broker.

Ejemplo:
```
POST http://[HOST]:[PUERTO]/ngsireceiver/v2/notify
```

Todos los requests recibidos en dicho endpoint se almacenan en una base de datos mongo con la siguiente configuración:

* BD: *buses*
* Colección: *events*

## Almacenamiento
El proyecto almacena todos los eventos enviados por Orion Context Broker en una base de datos MongoDB. De esta forma todos 
los datos de posiciones de omnibus quedan almacenados de forma persistentes, tanto para analizarlos como para utilizarlos 
de forma offline en el algorítmos de cálculo del TEA.

La elección de este motor de base de datos se fundo en las siguientes premisas:
* Orion Context Broker utiliza MongoDB, por lo tanto utilizar el mismo motor de base de datos reduce la complejidad tecnológica del 
proyecto y facilita y reduce costos en el despliegue.
* MongoDB es una base de datos Orientada a documentos, en principio "schema-free". Esta característica otorga de una gran
versatilidad a la solución a la hora de soportar cambios en el esquema de los datos enviados. Por ejemplo incorporar nuevos
atributos enviados por los ómnibus.

Por más información acerca de MongoDB y como levantarlo de forma fácil ver
[configuración mongo](./.docs/MONGO.md)

## Suscripción
El proyecto se comunica con Orion Context Broker a través de su mecanismo de suscripciones (ver [subscriptions](https://fiware-orion.readthedocs.io/en/develop/user/walkthrough_apiv2/#subscriptions)). 
De esta forma la componente desarrollada es notificada cada vez que llegan nuevos eventos de actualización de contextos. Siendo estos 
contextos los ómnibus para los cuales el simulador envia mediciones.

La suscripción, se crea mediante la API REST de Orion Context Broker con los valores que se describen a continuación:

* **notification.http.url**: URL al servicio que Orion Context Broker debe invocar cada vez que se produce una actualización de contexto y que además verifica la condición de la
suscripción. En este caso debemos especificar la URL completa al servicio de este proyecto. Para fijar ideas, si se tiene la VM del desafío y esta componente se encuentra desplegada
en el host anfitrión, es necesario completar esta URL con la dirección *IP* asignada a la VM, el puerto utilizado para configurar el servidor Tomcat donde se despliega este proyecto y por último 
el path hacia el servicio. 

* **subject.entities.type**: EntityType para el cual se crea la suscripción (la suscripción es a los contextos de un EntityType particular, dentro de un Fiware-Service y Fiware-ServicePath determinados). 
 En el caso del desafío, el EntityType utilizado por el simulador es: *"Bus"*.

* Fiware-Service y Fiware-ServicePath deben ir vacios ya que no se estan utilizando en este caso.


* subject.entities.idPattern: Patrón de identificadores para los contextos a los cuales se suscribe la aplicación. En este caso, se suscribe a cambios en cualquier contexto, por lo que se 
utiliza el patrón: ".*".

* condition.attrs: El campo condition es utilizado para definir una condición a cumplir antes de notificar por un cambio de contexto en Orion.
De esta forma se puede evitar enviar notificaciones por ejemplo si un ómnibus reporta un mensaje y no se producen cambios por ejemplo en los atributos "latitud" y "longitud". En este caso
dejamos la lista de atributos de la condición vacía, para indicarle a Orion que se quiere notificar al suscriptor cada vez
que se produce una actualización del Contexto, no importa si los valores cambiaron o no.

A continuación se muestra un ejemplo de creación de la suscripción mediante la API REST de Orion y utilizando 
la herramienta curl. Vale la pena destacar, que Orion tiene dos versiones de API: v1 y v2. Por considerarse actualmente deprecada
la versión 1, estamos utilizando la versión 2.

```
curl -X POST \
  http://192.168.56.101:1026/v2/subscriptions \
  -H 'Content-Type: application/json' \
  -d '{
  "subject": {
    "entities": [
      {
        "idPattern": ".*",
        "type": "Bus"
      }
    ],
    "condition": {
    	"attrs": []
    }
  },
  "notification": {
    "http": {
        "url": "http://192.168.56.1:8080/rest/v2/notify"
    }
  }
}'
```

Finalmente las sucripciones tienen un tiempo de expiración. En este caso y por tratarse de un
desafío, no se indica expiración alguna al momento de crear la suscripción. De esta forma Orion asume que es una suscripción 
que nunca expira.

En el caso de este proyecto, la suscripción se crea automáticamente al iniciar la aplicación (ver clase NGSIReceiverApplication.java).

## Configuración
El proyecto cuenta con el siguiente set de propiedades de configuración. Las mismas deben ser correctamente definidas para 
lograr un despliegue correcto de la solución. 

* **URL ORION**: URL a donde se encuentra público el servicio de Orion Context Broker. Puede definirse
mediante la variable de entorno **ORION_URL** o mediante la propiedad *uy.com.earlgrey.ngsireceiver.orionUrl* del archivo application.properties.

* **URL Core del Servicio Tea**: URL a donde se encuentra el servicio core del desafío. Puede definirse mediante
la variable de entorno **TEA_CORE_URL** o la propiedad de proyecto *uy.com.earlgrey.ngsireceiver.teaCoreUrl*.

* **URL Del servicio receptor (propio proyecto)**: Es la URL del propio servicio receptor (API de este proyecto). Se necesita para
pasarla como atributo dentro de la suscripción creada en Orion. Puede definirse mediante la variable de entorno **SERVICIO_URL**
 o mediante la propiedad de *proyecto uy.com.earlgrey.ngsireceiver.Url*.

* **Host de la BD MongoDB**: Host donde se encuentra la BD MongoDB. Puede definirse mediante la variable de entorno **MONGO_HOST**
 o mediante la propiedad de *uy.com.earlgrey.ngsireceiver.mongo.host*.

* **Puerto de la BD MongoDB**: Puerto del servidor MongoDB. Puede definirse mediante la variable de entorno **MONGO_PORT**
 o mediante la propiedad de *uy.com.earlgrey.ngsireceiver.mongo.port*.

El manejo de la carga de las propiedades del proyecto es realizado por la clase ConfigController.java. Esta clase se encarga de 
cargar todas las propiedades definidas en el archivo application.properties del proyecto y remplazar sus valores por los 
valores definidos en las respectivas variables de entorno, en caso de estar definidas.


 ## Dependencias
 El proyecto necesita de las siguientes dependencias:
 
 * [Apache Maven](https://maven.apache.org/): El proyecto utiliza Apache Maven para la gestión de dependencias y compilación.
 * [JDK 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html): Para la compilación del proyecto es necesario tener 
 instalado el Java Development Kit versión 8.
 * [Apache Tomcat 8.5](https://tomcat.apache.org/download-80.cgi#8.5.34): El despliegue de la aplicación se realiza en el servidor 
 Apache Tomcat 8.5.
   
 ## Build
 Para empaquetar el proyecto como WAR, ejecutar el siguiente comando Maven posicionado en la carpeta raiz del proyecto. 
 
 El war es generado
 utilizando el Plugin [Apache Maven WAR](https://maven.apache.org/plugins/maven-war-plugin/usage.html) y se genera dentro del
 siguiente directorio (relativo a la raiz del proyecto): `target/`
 
 ```
 mvn clean compile package
 ```
 
## Ejecución
1. Compilar el proyecto como se indica en la sección [Build](#Build).
2. Copiar el artefacto generado *ngsi-receiver.war* dentro del directorio ROOT del servidor Tomcat. El propio proyecto tiene configurado
como CONTEXT ROOT la URL "/ngsireceiver".
3. Levantar servidor

Puede verificarse un correcto desploy a través del servicio "status":

```
GET http://[HOST]:[PUERTO]/ngsireceiver/status
```
  
