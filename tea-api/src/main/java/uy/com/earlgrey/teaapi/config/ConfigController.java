package uy.com.earlgrey.teaapi.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by eviotti on  15/10/18.
 */
public class ConfigController {

    private final static String TEA_CORE_URL_ENV_NAME = "TEA_CORE_URL";

    private Logger logger = LoggerFactory.getLogger(ConfigController.class);

    private String teaCoreUrl;

    private Properties properties = new Properties();

    public ConfigController() {

        InputStream input = null;

        try {

            final String filename = "application.properties";
            input = ConfigController.class.getClassLoader().getResourceAsStream(filename);
            if (input == null) {
                logger.error("Error loading configuration file {}", filename);
            }

            //load a properties file from class path, inside static method
            properties.load(input);

            // TEA CORE URL
            if (System.getenv(TEA_CORE_URL_ENV_NAME) != null) {
                teaCoreUrl = System.getenv(TEA_CORE_URL_ENV_NAME);
            } else {
                teaCoreUrl = properties.getProperty("uy.com.earlgrey.teaapi.teaCore.Url");
            }

        } catch (IOException ex) {
            logger.error("Error loading configuration file ");
        }
    }

    public String getTeaCoreUrl() {
        return teaCoreUrl;
    }

    @Override
    public String toString() {
        return "ConfigController{" +
                "teaCoreUrl='" + teaCoreUrl + '\'' +
                '}';
    }
}
