package uy.com.earlgrey.teaapi.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uy.com.earlgrey.teaapi.dto.TeaResponse;
import uy.com.earlgrey.teaapi.services.TeaService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by eviotti on  15/10/18.
 */
@Path("/")
public class TeaController {

    private Logger logger = LoggerFactory.getLogger(TeaController.class);

    public TeaController() {
    }
    
    @GET
    @Path("/status")
    @Produces("text/plain")
    public String status() {
    	return "OK";
    }

    @GET
    @Produces("application/json")
    @Path("/nextBus/{idLinea}/{idParada}")
    public Response nextBus(@PathParam("idLinea") String idLinea, @PathParam("idParada") int idParada) {

        logger.info("======> Servicio TEA: nextBus para linea={}, parada={} ", idLinea, idParada);

        TeaService teaService = new TeaService();
        TeaResponse result = teaService.getTeaCorePrediction(idLinea, idParada);

        ObjectMapper mapper = new ObjectMapper();
        try {
            String resultAsJsonString = mapper.writeValueAsString(result);
            return Response.status(200).entity(resultAsJsonString).build();
        } catch (JsonProcessingException e) {
            logger.error("Error converting tea result as json string {}", e);
            return Response.status(500).entity("Error convirtiendo respuesta").build();
        }

    }
}
