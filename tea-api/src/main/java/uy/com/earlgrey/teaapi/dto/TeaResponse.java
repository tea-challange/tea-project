package uy.com.earlgrey.teaapi.dto;

/**
 * Created by eviotti on  15/10/18.
 */
public class TeaResponse {

    private String idLinea;
    private int idParada;
    private int idBus;
    private LocationDto location;
    private double tea;

    public TeaResponse() {
    }

    public TeaResponse(String idLinea, int idParada, int idBus, LocationDto location, double tea) {
        this.idLinea = idLinea;
        this.idParada = idParada;
        this.idBus = idBus;
        this.location = location;
        this.tea = tea;
    }

    public String getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(String idLinea) {
        this.idLinea = idLinea;
    }

    public int getIdParada() {
        return idParada;
    }

    public void setIdParada(int idParada) {
        this.idParada = idParada;
    }

    public int getIdBus() {
        return idBus;
    }

    public void setIdBus(int idBus) {
        this.idBus = idBus;
    }

    public LocationDto getLocation() {
        return location;
    }

    public void setLocation(LocationDto location) {
        this.location = location;
    }

    public double getTea() {
        return tea;
    }

    public void setTea(double tea) {
        this.tea = tea;
    }

    @Override
    public String toString() {
        return "TeaResponse{" +
                "idLinea='" + idLinea + '\'' +
                ", idParada=" + idParada +
                ", idBus=" + idBus +
                ", location=" + location +
                ", tea=" + tea +
                '}';
    }
}
