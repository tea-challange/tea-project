package uy.com.earlgrey.teaapi.application;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uy.com.earlgrey.teaapi.config.ConfigController;
import uy.com.earlgrey.teaapi.controllers.TeaController;

@ApplicationPath("/")
public class TeaApplication extends Application {
	private Set<Object> singletons = new HashSet<Object>();

	private Logger logger = LoggerFactory.getLogger(TeaApplication.class);

	public TeaApplication() {
		// Register TEA service
		singletons.add(new TeaController());

		// Logs actual loaded configuration
		ConfigController config = new ConfigController();
		logger.info("CONFIGURATION \n" + config.toString());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
