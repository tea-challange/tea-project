package uy.com.earlgrey.teaapi.services;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uy.com.earlgrey.teaapi.config.ConfigController;
import uy.com.earlgrey.teaapi.dto.LocationDto;
import uy.com.earlgrey.teaapi.dto.TeaResponse;

import javax.ws.rs.ServiceUnavailableException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by eviotti on  15/10/18.
 */
public class TeaService {

    private Logger logger = LoggerFactory.getLogger(TeaService.class);

    public TeaResponse getDummyTeaService(String idLinea, int idParada) {
        logger.info("======> TeaService: getDummyTeaService ");

        TeaResponse response = new TeaResponse();

        response.setIdLinea(idLinea);
        response.setIdParada(idParada);
        response.setIdBus(1);
        response.setTea(-1);
        List<Double> coordinates = new ArrayList<>();
        coordinates.add(-56.19539);
        coordinates.add(-34.90608);
        response.setLocation(new LocationDto("Point", coordinates));
        return response;
    }

    public TeaResponse getTeaCorePrediction(String idLinea, int idParada) {
        logger.info("======> TeaCoreService: sendNotificationEvent ");

        ConfigController config = new ConfigController();
        String teaCoreURL = config.getTeaCoreUrl();

        String uri = teaCoreURL + String.format("/query/%s/%s", idLinea, idParada);

        try {

            ClientRequest request = new ClientRequest(uri);
            request.accept("application/json");
            ClientResponse<TeaResponse> response = request.get(TeaResponse.class);

            if (response.getStatus() != 200) {
                logger.error("Error invoking to tea core. msg={}, statis={}", response.getEntity().toString(),
                        response.getStatusInfo().toString());
            }

            return response.getEntity();

        } catch (Exception e) {
            logger.error("Error connecting to TEA Core {}", e);
            throw new ServiceUnavailableException();
        }
    }
}
