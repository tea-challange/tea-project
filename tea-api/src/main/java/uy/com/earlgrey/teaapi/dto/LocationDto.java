package uy.com.earlgrey.teaapi.dto;

import java.util.List;

/**
 * Created by eviotti on  15/10/18.
 */
public class LocationDto {
    private String type;
    private List<Double> coordinates;

    public LocationDto() {
    }

    public LocationDto(String type, List<Double> coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Double> coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "LocationDto{" +
                "type='" + type + '\'' +
                ", coordinates=" + coordinates +
                '}';
    }
}
