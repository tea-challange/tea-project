 # tea-api

 ## Sobre el proyecto
 Este proyecto corresponde a la API de servicios rest que incluyen al servicio tea para el desafio de la IMM

 ## Servicios
 Se listan los servicios disponibles:
 
 #### /status
 Devuelve el status de la aplicación: "OK" si esta todo bien.
 
 Ejemplo:
  ```
  GET http://[HOST]:[PUERTO]/status
  ```

 #### /nextBus/{idLinea}/{idParada}
 Servicio TEA, devuelve el tiempo estimado de arribo del siguiente bus, para un código de línea y una parada determinada.
  
 Ejemplo:
   ```
   GET http://[HOST]:[PUERTO]/nextBus/{idLinea}/{idParada}
   ```
   
 ## Configuración
 El proyecto cuenta con el siguiente set de propiedades de configuración. Las mismas deben ser correctamente definidas para 
 lograr un despliegue correcto de la solución. 
 
 * **URL Core del Servicio Tea**: URL a donde se encuentra el servicio core del desafío. Puede definirse mediante
   la variable de entorno **TEA_CORE_URL** o la propiedad de proyecto *uy.com.earlgrey.teaapi.teaCore.Url*.

  
 ## Dependencias
 El proyecto necesita de las siguientes dependencias:
 
 * [Apache Maven](https://maven.apache.org/): El proyecto utiliza Apache Maven para la gestión de dependencias y compilación.
 * [JDK 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html): Para la compilación del proyecto es necesario tener 
 instalado el Java Development Kit versión 8.
 * [Apache Tomcat 8.5](https://tomcat.apache.org/download-80.cgi#8.5.34): El despliegue de la aplicación se realiza en el servidor 
 Apache Tomcat 8.5.
   
 ## Build
 Para empaquetar el proyecto como WAR, ejecutar el siguiente comando Maven posicionado en la carpeta raiz del proyecto. 
 
 El war es generado
 utilizando el Plugin [Apache Maven WAR](https://maven.apache.org/plugins/maven-war-plugin/usage.html) y se genera dentro del
 siguiente directorio (relativo a la raiz del proyecto): `target/`
 
 ```
 mvn clean compile package
 ```
 
 ## Ejecución
 1. Compilar el proyecto como se indica en la sección [Build](#Build).
 2. Copiar el artefacto generado *tea-api.war* dentro del directorio ROOT del servidor Tomcat. El propio proyecto tiene configurado
 como CONTEXT ROOT la URL raíz "/".
 3. Levantar servidor
 
 Puede verificarse un correcto desploy a través del servicio "status":
 
 ```
 GET http://[HOST]:[PUERTO]/status
 ```
